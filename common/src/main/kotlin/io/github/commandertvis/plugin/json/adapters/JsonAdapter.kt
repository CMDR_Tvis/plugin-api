package io.github.commandertvis.plugin.json.adapters

import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import kotlin.reflect.KClass

/**
 * Represents an adapter to serialize and deserialize JSON.
 */
public interface JsonAdapter {
    /**
     * Tries to instantiate [T] by contents of JSON string.
     *
     * @param T the type of object to deserialize.
     * @param string the JSON string.
     * @param classOfT the class reference of [T].
     * @return new [T] instance or null.
     */
    public fun <T : Any> fromJson(string: String, classOfT: KClass<out T>): T?

    /**
     * Tries to instantiate [T] by contents of JSON stream.
     *
     * @param T the type of object to deserialize.
     * @param stream the JSON stream.
     * @param classOfT the class reference of [T].
     * @return new [T] instance or null.
     */
    @Throws(IOException::class)
    public fun <T : Any> fromJson(stream: InputStream, classOfT: KClass<out T>): T? =
        fromJson(stream.readBytes().decodeToString(), classOfT)

    /**
     * Tries to create a JSON string by the object's contents.
     *
     * @param any the object to serialize.
     */
    public fun toJson(any: Any): String?

    /**
     * Tries to create a JSON string by the object's contents and send it to stream.
     *
     * @param any the object to serialize.
     */
    @Throws(IOException::class)
    public fun toJson(any: Any, stream: OutputStream): Unit =
        toJson(any)?.let { stream.write(it.encodeToByteArray()) } ?: Unit
}
