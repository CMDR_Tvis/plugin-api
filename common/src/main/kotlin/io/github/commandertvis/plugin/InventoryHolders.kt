package io.github.commandertvis.plugin

import org.bukkit.Bukkit
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.InventoryHolder

/**
 * Creates an empty inventory of type [InventoryType.CHEST] with the specified size.
 *
 * @param size a multiple of 9 as the size of inventory to create.
 * @return a new inventory.
 * @throws IllegalArgumentException if the size is not a multiple of 9.
 */
public fun InventoryHolder?.createInventory(size: Int): Inventory = Bukkit.createInventory(this, size)

/**
 * Creates an empty inventory with the specified type and title. If the type is [InventoryType.CHEST], the new
 * inventory has a size of 27; otherwise the new inventory has the normal size for its type.
 *
 * It should be noted that some inventory types do not support titles and may not render with said titles on the
 * Minecraft client.
 *
 * [InventoryType.WORKBENCH] will not process crafting recipes if created with this method. Use
 * [org.bukkit.entity.Player.openWorkbench] instead.
 *
 * [InventoryType.ENCHANTING] will not process item stacks for possible enchanting results. Use
 * [org.bukkit.entity.Player.openEnchanting] instead.
 *
 * @param type the type of inventory to create.
 * @return a new inventory.
 * @throws IllegalArgumentException if the [InventoryType] cannot be viewed.
 * @see InventoryType.isCreatable
 */
public fun InventoryHolder?.createInventory(type: InventoryType): Inventory = server.createInventory(this, type)

/**
 * Creates an empty inventory of type [InventoryType.CHEST] with the specified size and title.
 *
 * @param size a multiple of 9 as the size of inventory to create
 * @param title the title of the inventory, displayed when inventory is viewed
 * @return a new inventory
 * @throws IllegalArgumentException if the size is not a multiple of 9
 */
public fun InventoryHolder?.createInventory(size: Int, title: String): Inventory =
    server.createInventory(this, size, title)

/**
 * Creates an empty inventory with the specified type and title. If the type
 * is [InventoryType.CHEST], the new inventory has a size of 27; otherwise the new inventory has the normal size for
 * its type.
 *
 * It should be noted that some inventory types do not support titles and may not render with said titles on the
 * Minecraft client.
 *
 * [InventoryType.WORKBENCH] will not process crafting recipes if
 * created with this method. Use [org.bukkit.entity.Player.openWorkbench] instead.
 *
 * [InventoryType.ENCHANTING] will not process {@link ItemStack}s
 * for possible enchanting results. Use [org.bukkit.entity.Player.openEnchanting] instead.
 *
 * @param type the type of inventory to create.
 * @param title the title of the inventory, to be displayed when it is viewed.
 * @return a new inventory.
 * @throws IllegalArgumentException if the [InventoryType] cannot be viewed.
 *
 * @see InventoryType.isCreatable
 */
public fun InventoryHolder?.createInventory(type: InventoryType, title: String): Inventory =
    server.createInventory(this, type, title)
