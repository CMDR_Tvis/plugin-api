package io.github.commandertvis.plugin

import org.bukkit.BanList

/**
 * Gets a ban list for the supplied [BanList.Type].
 *
 * Bans by name are no longer supported and this method will return `null` when trying to request them. The replacement
 * is bans by UUID.
 *
 * @return a ban list of the specified type.
 */
public inline val BanList.Type.list: BanList
    get() = server.getBanList(this)
