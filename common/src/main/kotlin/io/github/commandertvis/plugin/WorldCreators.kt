package io.github.commandertvis.plugin

import org.bukkit.World
import org.bukkit.WorldCreator

/**
 * Creates or loads a world with the given name using the specified options. If the world is already loaded, it will
 * just return the world.
 *
 * @return the newly created or loaded world.
 */
public fun WorldCreator.create(): World = server.createWorld(this)!!
