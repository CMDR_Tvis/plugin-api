package io.github.commandertvis.plugin

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.OfflinePlayer
import org.bukkit.World
import org.bukkit.command.PluginCommand
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.util.*
import org.bukkit.ChatColor as BukkitChatColor

/**
 * Finds a [Material] with key of this [String].
 *
 * @return the [Material] with key of this [String] or `null`.
 */
@Suppress("DEPRECATION")
public fun findMaterialByKey(key: String): Material? = Material.values().find {
    it.key == (key.substringBeforeLast(':') to key.substringAfterLast(':')).toNamespacedKey()
}

/**
 * Returns an enum entry with specified name or `null`.
 *
 * @param name the name of value.
 * @return the according value or `null`.
 */
public inline fun <reified T : Enum<T>> enumValueOrNullOf(name: String): T? =
    runCatching<T> { enumValueOf(name) }.getOrNull()


/**
 * Finds a [World] with name of this [String].
 *
 * @param name the name of the world to find.
 * @return the [World] with name of this [String] or `null`.
 */
public fun findWorld(name: String): World? = server.getWorld(name)

/**
 * Replaces multiple placeholders in this [String].
 *
 * @param pairs the pairs, where [Pair.first] is the key and [Pair.second] is the value.
 * @return this string with replaced placeholders.
 */
public fun String.placeholders(vararg pairs: Pair<String, Any>): String {
    var string = this
    pairs.forEach { string = string.placeholder(it) }
    return string
}

/**
 * Finds a [OfflinePlayer] with the nickname of this [String]. This function doesn't convert the UUID like
 * [Bukkit.getOfflinePlayer].
 *
 * @param name the name of the player to find.
 * @return either player with nickname equal to this string, or `null` if such a player doesn't exist.
 */
public fun findOfflinePlayer(name: String): OfflinePlayer? = server.offlinePlayers.find { it.name == name }

/**
 * Finds a [Player] (online player) with nickname of this [String].
 *
 * @param name the name of the player to find.
 * @return either player with nickname equal to this string, or `null` if such a player doesn't exist.
 */
public fun findPlayer(name: String): Player? = server.onlinePlayers.find { it.name.equals(name, true) }

/**
 * Constructs a new [UUID] from this [String].
 *
 * @return a new [UUID].
 */
public fun String.toUuid(): UUID = UUID.fromString(this)

/**
 * Constructs new [UUID] from this [String].
 *
 * @return a new [UUID] or `null`.
 */
public fun String.toUuidOrNull(): UUID? = try {
    toUuid()
} catch (ignored: IllegalStateException) {
    null
}

/**
 * Broadcasts a message to all players.
 *
 * @param message the message to broadcast.
 * @return the number of players.
 */
public fun broadcast(message: String): Int = server.broadcastMessage(message)

/**
 * Broadcasts a message to players with a certain [Permission].
 *
 * @param message the message to broadcast.
 * @param permission the permission of receivers.
 * @return the number of players.
 */
public fun broadcast(message: String, permission: Permission): Int =
    broadcast(message, permission.name)

/**
 * Broadcasts a message to players with a certain permission.
 *
 * @param message the message to broadcast.
 * @param permission the permission of receivers.
 * @return the number of players.
 */
public fun broadcast(message: String, permission: String): Int = server.broadcast(message, permission)

/**
 * Formats messages by Bukkit color codes.
 *
 * @return the string where `&` color codes are replaced with \`u00A7` color codes.
 */
public fun String.colorize(): String = BukkitChatColor.translateAlternateColorCodes('&', this)

/**
 * Attempts to match any players with the given name, and returns a list of all possibly matches. This list is not
 * sorted in any particular order. If an exact match is found, the returned list will only contain a single result.
 *
 * @param name the name or partial name of the player to find.
 * @return list of all possible players.
 */
public fun matchPlayer(name: String): List<Player> = server.matchPlayer(name)

/**
 * Finds a [PluginCommand] with name of this [String].
 *
 * @param name the name of the command to retrieve.
 * @return the [PluginCommand] with name of this [String] or `null`.
 */
public fun findPluginCommand(name: String): PluginCommand? = server.getPluginCommand(name)

/**
 * Replaces %placeholders% with values.
 *
 * @param key the key of the placeholder.
 * @param value the value to replace with.
 * @return string where all the placeholders with the values.
 */
public fun String.placeholder(key: String, value: Any): String = replace("%$key%", value.toString())

/**
 * Replaces %placeholders% with values.
 *
 * @param pair [Pair], where [Pair.first] is the key and [Pair.second] is the value.
 * @return string where all the placeholders with the values.
 */
public fun String.placeholder(pair: Pair<String, Any>): String = placeholder(pair.first, pair.second)
