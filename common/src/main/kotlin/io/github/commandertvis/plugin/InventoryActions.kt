package io.github.commandertvis.plugin

import org.bukkit.event.inventory.InventoryAction

/**
 * Checks is this [InventoryAction] represents placing any quantity of items.
 *
 * @return is this [InventoryAction] represents placing any quantity of items.
 */
public fun InventoryAction.isPlaceAction(): Boolean =
    this == InventoryAction.PLACE_ALL || this == InventoryAction.PLACE_ONE || this == InventoryAction.PLACE_SOME

/**
 * Checks is this [InventoryAction] represents picking any quantity of items up.
 *
 * @return is this [InventoryAction] represents picking any quantity of items up.
 */
public fun InventoryAction.isPickupAction(): Boolean = this == InventoryAction.PICKUP_ONE
        || this == InventoryAction.PICKUP_SOME
        || this == InventoryAction.PICKUP_HALF
        || this == InventoryAction.PICKUP_ALL
