package io.github.commandertvis.plugin

import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.Recipe

/**
 * Gets a list of all recipes for a given item. The stack size is ignored in comparisons. If the durability is -1, it
 * will match any data value.
 *
 * @return a list of recipes with the given result.
 */
public inline val ItemStack.recipes: List<Recipe>
    get() = server.getRecipesFor(this)

/**
 * Returns is this stack's amount equals to its maximal stack size.
 *
 * @return is this stack's amount equals to its maximal stack size.
 */
public fun ItemStack.isFilledUp(): Boolean = maxStackSize == amount
