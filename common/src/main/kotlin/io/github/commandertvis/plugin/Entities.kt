package io.github.commandertvis.plugin

import org.bukkit.entity.Entity
import org.bukkit.entity.LivingEntity

/**
 * Represents if gravity influence on this [LivingEntity].
 */
public inline var Entity.gravity: Boolean
    get() = hasGravity()
    set(value) = setGravity(value)

/**
 * Unique ID of this [Entity] as [String].
 */
public inline val Entity.entityUuidString: String
    get() = uniqueId.toString()
