@file:Suppress("DEPRECATION")

package io.github.commandertvis.plugin

import org.bukkit.Material
import org.bukkit.boss.BarColor
import org.bukkit.boss.BarStyle
import org.bukkit.boss.BossBar
import org.bukkit.boss.KeyedBossBar
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta
import org.bukkit.material.MaterialData
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Constructs a [BossBar] and applies some data there.
 *
 * @param title the title of the boss bar.
 * @param color the color of the boss bar.
 * @param style the style of the boss bar.
 * @param block the extension function to apply.
 * @return the new [BossBar] reference.
 */
public inline fun bossBar(title: String, color: BarColor, style: BarStyle, block: BossBar.() -> Unit): BossBar {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return server.createBossBar(title, color, style).apply(block)
}

/**
 * Constructs an [ItemStack] and applies an extension function there
 *
 * @param material the material of the stack.
 *
 * @param block the extension function to apply, where receiver is a new [ItemStack] and meta parameter is receiver's
 * [ItemStack.getItemMeta].
 *
 * @return the new stack reference.
 */
public inline fun itemStack(material: Material, amount: Int = 1, block: ItemStack.() -> Unit): ItemStack {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return ItemStack(material, amount).apply(block)
}

/**
 * Constructs an [ItemStack] with typed meta and applies an extension function there.
 *
 * @param T type of meta of the [material].
 * @param material the material of the stack.
 *
 * @param block the extension function to apply, where receiver is a new [ItemStack] and meta parameter is receiver's
 * [ItemStack.getItemMeta].
 *
 * @throws ClassCastException when the [Material] doesn't match to [T].
 * @return the new [ItemStack] reference.
 */
@Suppress("UNCHECKED_CAST")
public inline fun <reified T : ItemMeta?> itemStack(
    material: Material,
    amount: Int = 1,
    block: ItemStack.(meta: T?) -> Unit
): ItemStack {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    return ItemStack(material, amount).apply {
        val meta = itemMeta
        block(meta as T?)
        itemMeta = meta
    }
}

/**
 * Constructs an [ItemStack] and applies an extension function there
 *
 * @param material the material data of the stack.
 *
 * @param block the extension function to apply, where receiver is a new [ItemStack] and meta parameter is receiver's
 * [ItemStack.getItemMeta].
 *
 * @return the new stack reference.
 */
@Deprecated("all usage of MaterialData is deprecated and subject to removal")
public inline fun itemStack(material: MaterialData, amount: Int = 1, block: ItemStack.() -> Unit): ItemStack {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return material.toItemStack(amount).apply(block)
}

/**
 * Constructs an [ItemStack] with typed meta and applies an extension function there.
 *
 * @param T type of meta of the [material].
 * @param material the material data of the stack.
 *
 * @param block the extension function to apply, where receiver is a new [ItemStack] and meta parameter is receiver's
 * [ItemStack.getItemMeta].
 *
 * @throws ClassCastException when the [Material] doesn't match to [T].
 * @return the new [ItemStack] reference.
 */
@Deprecated("all usage of MaterialData is deprecated and subject to removal")
@Suppress("UNCHECKED_CAST", "DEPRECATION")
public inline fun <reified T : ItemMeta?> itemStack(
    material: MaterialData,
    amount: Int = 1,
    block: ItemStack.(meta: T?) -> Unit
): ItemStack {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    return material.toItemStack(amount).apply {
        val meta = itemMeta
        block(meta as T?)
        itemMeta = meta
    }
}

/**
 * Constructs a [org.bukkit.boss.KeyedBossBar] and applies some data there.
 *
 * @param key the key of the boss bar.
 * @param title the title of the boss bar.
 * @param color the color of the boss bar.
 * @param style the style of the boss bar.
 * @param block the extension function to apply.
 * @return the new [BossBar] reference.
 */
public inline fun keyedBossBar(
    key: Pair<String, String>,
    title: String,
    color: BarColor,
    style: BarStyle,
    block: BossBar.() -> Unit
): KeyedBossBar {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return server.createBossBar(key.toNamespacedKey(), title, color, style).apply(block)
}
