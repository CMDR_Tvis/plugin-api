package io.github.commandertvis.plugin

import org.bukkit.Bukkit
import org.bukkit.Server

/**
 * [org.bukkit.Server] object from [Bukkit.getIp].
 */
public inline val server: Server
    get() = Bukkit.getServer()
