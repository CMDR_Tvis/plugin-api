package io.github.commandertvis.plugin.json

import org.bukkit.plugin.java.JavaPlugin
import java.io.File

/**
 * Represents an additional configuration file, attached to a plugin, for example for data storage management - not
 * main settings.
 *
 * @param C the structure of this configuration.
 * @property name The name of this configuration.
 */
public abstract class AdditionalJsonConfiguration<C : Any> public constructor(
    public final override val defaultJsonConfig: C,
    private val plugin: JavaPlugin,
    public val name: String
) : JsonConfigurationManager<C> {

    /**
     * Lazy-pulled JSON configuration.
     */
    private val lazyJsonConfig = LazyJsonConfigurationValue<C, AdditionalJsonConfiguration<C>>()

    public override val jsonConfigFile: File
        get() = File(
            plugin.dataFolder.apply {
                if (!exists())
                    mkdirs()
            },
            "$name.json"
        ).apply {
            if (!exists())
                createNewFile()
        }

    public final override val jsonConfigFilePath: String
        get() = jsonConfigFile.path

    /**
     * Retrieves the configuration, parsed with [C] type from file contents.
     */
    public override var jsonConfig: C by lazyJsonConfig

    /**
     * Saves the current JSON configuration to the [jsonConfigFile].
     */
    public override fun saveConfig(): Unit = run { jsonConfig = checkNotNull(lazyJsonConfig.value) }

    /**
     * Saves the default JSON configuration to the [jsonConfigFile].
     */
    public override fun saveDefaultConfig(): Unit = run { jsonConfig }

    /**
     * Reloads the [jsonConfig] from the contents of [jsonConfigFile].
     */
    public override fun reloadConfig(): Unit = lazyJsonConfig.reload(this)
}
