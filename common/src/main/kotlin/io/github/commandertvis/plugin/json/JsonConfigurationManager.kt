package io.github.commandertvis.plugin.json

/**
 * Extends [JsonConfiguration] API with actions for objects, abstracting the work with the configurations.
 */
public interface JsonConfigurationManager<C : Any> : JsonConfiguration<C> {
    /**
     * The JSON configuration accessor. It is recommended being implemented with caching the value.
     */
    public var jsonConfig: C

    /**
     * Saves the [jsonConfig] to file.
     */
    public fun saveConfig()

    /**
     * Saves the default value of [jsonConfig] to file.
     */
    public fun saveDefaultConfig()

    /**
     * Reloads the [jsonConfig] from file.
     */
    public fun reloadConfig()
}
