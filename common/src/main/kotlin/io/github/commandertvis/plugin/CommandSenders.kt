package io.github.commandertvis.plugin

import org.bukkit.command.CommandException
import org.bukkit.command.CommandSender

/**
 * Dispatches a command on this server, and executes it if it exists.
 *
 * @param commandLine the command with arguments. Example: `test abc 123`.
 * @return returns false if no target is found.
 * @throws CommandException thrown when the executor for the given command fails with an unhandled exception.
 */
public fun CommandSender.command(commandLine: String): Boolean = server.dispatchCommand(this, commandLine)
