package io.github.commandertvis.plugin

import org.bukkit.entity.LivingEntity
import org.bukkit.potion.PotionEffect

/**
 * Represents if this [LivingEntity] has AI.
 */
public inline var LivingEntity.ai: Boolean
    get() = hasAI()
    set(value) = setAI(value)

/**
 * Removes all the active potion effects of this [LivingEntity].
 */
public fun LivingEntity.removePotionEffects(): Unit =
    activePotionEffects.asSequence().map(PotionEffect::getType).forEach(::removePotionEffect)
