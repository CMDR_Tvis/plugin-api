package io.github.commandertvis.plugin.json.adapters

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.io.InputStream
import java.io.OutputStream
import kotlin.reflect.KClass

/**
 * A [JsonAdapter] implementation, which uses [Gson] for serialization and deserialization.
 */
public open class GsonAdapter public constructor(
    /**
     * [Gson] instance, used by this adapter to deserialize and serialize objects.
     */
    protected val gson: Gson = GsonBuilder().run {
        setPrettyPrinting()
        serializeNulls()
        create()
    },
) : JsonAdapter {
    public override fun <T : Any> fromJson(stream: InputStream, classOfT: KClass<out T>): T? = try {
        stream.reader().use { gson.fromJson<T>(it, classOfT.java) }
    } catch (ignored: Exception) {
        null
    }

    public override fun toJson(any: Any, stream: OutputStream): Unit = stream.writer().use { gson.toJson(any, it) }

    public override fun <T : Any> fromJson(string: String, classOfT: KClass<out T>): T? = try {
        gson.fromJson<T>(string, classOfT.java)
    } catch (ignored: Exception) {
        null
    }

    public override fun toJson(any: Any): String? = gson.toJson(any)

    public companion object : GsonAdapter()
}
