package io.github.commandertvis.plugin

import org.bukkit.Material
import org.bukkit.NamespacedKey

/**
 * Converts this [Pair] as material key to [Material].
 *
 * For example:
 *
 * ```
 * findMaterialByKey("minecraft" to "diamond_ore")
 * ```
 *
 * @return the material with registry ID equal to this key, or null if such material doesn't exist.
 */
@Suppress("DEPRECATION")
public fun findMaterialByKey(namespaceAndKey: Pair<String, String>): Material? {
    Material.values().forEach { m -> m.key.takeIf { it == namespaceAndKey.toNamespacedKey() }?.let { return m } }
    return null
}

/**
 * Creates a new [NamespacedKey] with [NamespacedKey.namespace] as [Pair.first] and [NamespacedKey.key] as
 * [Pair.second]. Uses constructor for internal use only!
 */
@Suppress("DEPRECATION")
@Deprecated(message = "should never be used by plugins, for internal use only")
public fun Pair<String, String>.toNamespacedKey(): NamespacedKey = NamespacedKey(first, second)
