package io.github.commandertvis.plugin.json.adapters

import java.io.InputStream

/**
 * Tries to instantiate [T] by contents of JSON string.
 *
 * @param T the type of object to deserialize.
 * @param string the JSON string.
 * @return new [T] instance or null.
 */
public inline fun <reified T : Any> JsonAdapter.fromJson(string: String): T? = fromJson(string, T::class)

/**
 * Tries to instantiate [T] by contents of JSON string.
 *
 * @param T the type of object to deserialize.
 * @param stream the JSON stream.
 * @return new [T] instance or null.
 */
public inline fun <reified T : Any> JsonAdapter.fromJson(stream: InputStream): T? = fromJson(stream, T::class)
