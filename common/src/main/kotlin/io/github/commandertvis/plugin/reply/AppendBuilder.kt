package io.github.commandertvis.plugin.reply

import org.bukkit.command.CommandSender
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Creates a [String] by [StringBuilder] in [AppendScope], where an extension function applied
 *
 * @param block the extension function to apply
 * @return the new [String]
 */
public inline fun appending(
    initial: StringBuilder = StringBuilder(),
    block: AppendScope<StringBuilder>.() -> Unit
): String {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return AppendScope(initial).apply(block).appendable.toString()
}

/**
 * Creates a [String] by [StringBuilder] in [AppendScope], where an extension function applied
 *
 * @param T the type of [Appendable] to use with [AppendScope]
 * @param block the extension function to apply
 * @return the new [String]
 */
public inline fun <reified T : Appendable> appending(initial: T, block: AppendScope<T>.() -> Unit): String {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return AppendScope(initial).apply(block).appendable.toString()
}

/**
 * Creates a [String] by [StringBuilder] in [AppendScope], where an extension function applied, sends it to a
 * [CommandSender]
 *
 * @param block the extension function to apply
 * @return the new [String]
 */
public inline fun CommandSender.replyAppending(
    builder: StringBuilder = StringBuilder(),
    block: AppendScope<StringBuilder>.() -> Unit
) {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    sendMessage(appending(builder, block))
}

/**
 * Creates a [String] by [StringBuilder] in [AppendScope], where an extension function applied, sends it to a
 * [CommandSender]
 *
 * @param T the type of [Appendable] to use with [AppendScope]
 * @param block the extension function to apply
 * @return the new [String]
 */
public inline fun <reified T : Appendable> CommandSender.replyAppending(builder: T, block: AppendScope<T>.() -> Unit) {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    sendMessage(appending(builder, block))
}
