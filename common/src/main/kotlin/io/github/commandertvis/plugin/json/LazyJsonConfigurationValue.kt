package io.github.commandertvis.plugin.json

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Represents a simple lazy-pulled JSON configuration value.
 *
 * @param C the structure of configuration to store with this [LazyJsonConfigurationValue].
 * @param R [JsonConfiguration] object to delegate by this [LazyJsonConfigurationValue] with.
 */
public class LazyJsonConfigurationValue<C : Any, R : JsonConfiguration<C>> internal constructor() :
    ReadWriteProperty<R, C> {

    @PublishedApi
    internal var value: C? = null

    /**
     * Updates the value, stored in this [LazyJsonConfigurationValue].
     */
    public fun reload(thisRef: R): Unit = run { value = thisRef.getJsonConfiguration() }

    /**
     * Loads the value from [JsonConfiguration.jsonConfigFile] or returns the cached one.
     *
     * @param thisRef [JsonConfiguration] that manages this value of [C].
     * @param property the delegated property reference.
     * @return the loaded from file value or the cached one.
     */
    public override fun getValue(thisRef: R, property: KProperty<*>): C {
        if (value == null)
            value = thisRef.getJsonConfiguration()

        return value!!
    }

    /**
     * Saves the value to [JsonConfiguration.jsonConfigFile].
     *
     * @param thisRef [JsonConfiguration] that manages this value of [C].
     * @param property the delegated property reference.
     */
    public override fun setValue(thisRef: R, property: KProperty<*>, value: C) {
        this.value = value
        thisRef.saveJsonConfiguration(value)
    }

    /**
     * Returns the string representation of the stored value.
     *
     * @return the string representation of the stored value.
     */
    public override fun toString(): String = value.toString()
}
