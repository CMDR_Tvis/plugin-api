package io.github.commandertvis.plugin.json

import io.github.commandertvis.plugin.json.adapters.GsonAdapter
import io.github.commandertvis.plugin.json.adapters.JsonAdapter
import java.io.File
import java.io.IOException

/**
 * Represents a typed JSON configuration manager.
 *
 * @param C the structure of this configuration.
 */
public interface JsonConfiguration<C : Any> {
    /**
     * The adapter instance for JSON conversion. It returns [GsonAdapter.Companion] reference by default.
     */
    public val adapter: JsonAdapter
        get() = GsonAdapter

    /**
     * The file, where this configuration stored.
     *
     * @throws IOException when an error with file IO occurs.
     */
    public val jsonConfigFile: File
        @Throws(IOException::class)
        get() = File(jsonConfigFilePath).apply { if (!exists()) createNewFile() }

    /**
     * The path to [jsonConfigFile].
     */
    public val jsonConfigFilePath: String

    /**
     * The default [C] instance.
     */
    public val defaultJsonConfig: C

    /**
     * Tries to retrieve [C] configuration from [jsonConfigFile] and if it is impossible saves [defaultJsonConfig] to
     * the file and returns it.
     *
     * @return [C] type configuration, deserialized from file contents, or [defaultJsonConfig].
     */
    @Throws(IOException::class)
    public fun getJsonConfiguration(): C {
        jsonConfigFile
            .inputStream()
            .use { `is` -> adapter.fromJson(`is`, defaultJsonConfig::class)?.let { c -> return c } }

        saveJsonConfiguration(defaultJsonConfig)
        return defaultJsonConfig
    }

    /**
     * Overrides the [jsonConfigFile] contents by the needed structure of an object.
     *
     * @param value the initial value to write.
     * @throws IOException when an error with file IO occurs.
     */
    @Throws(IOException::class)
    public fun saveJsonConfiguration(value: C): Unit = jsonConfigFile.outputStream().use { adapter.toJson(value, it) }
}
