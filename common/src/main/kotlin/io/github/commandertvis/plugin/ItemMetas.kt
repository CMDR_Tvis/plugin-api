package io.github.commandertvis.plugin

import org.bukkit.inventory.meta.ItemMeta

/**
 * Gets or creates new lore from [ItemMeta].
 *
 * @param defaultValue the value to save in this meta if there's no lore.
 * @return either new lore or lore in this meta.
 */
public fun ItemMeta.getOrCreateLore(defaultValue: MutableList<String> = mutableListOf()): MutableList<String> {
    if (!hasLore())
        lore = defaultValue

    return lore!!
}
