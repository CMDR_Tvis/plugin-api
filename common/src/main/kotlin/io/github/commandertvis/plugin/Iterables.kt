package io.github.commandertvis.plugin

import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

/**
 * Sends a message to each player of this [Iterable].
 *
 * @param message the message to be sent.
 */
public fun Iterable<CommandSender>.broadcast(message: String): Unit = forEach { it.sendMessage(message) }

/**
 * Kicks each player of this [Iterable].
 *
 * @param message the kick message.
 */
public fun Iterable<Player>.kickEveryone(message: String): Unit = forEach { it.kickPlayer(message) }
