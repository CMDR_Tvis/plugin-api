package io.github.commandertvis.plugin

import org.bukkit.OfflinePlayer
import org.bukkit.entity.Entity
import org.bukkit.entity.Player
import java.util.*

/**
 * Converts this [UUID] to [Entity].
 *
 * @return either an entity with certain UUId, or `null` if such entity doesn't exist.
 */
public fun findEntity(uuid: UUID): Entity? = server.getEntity(uuid)

/**
 * Converts this [UUID] to [OfflinePlayer].
 *
 * @return an offline player instance with certain UUID.
 */
public fun findOfflinePlayer(uuid: UUID): OfflinePlayer = server.getOfflinePlayer(uuid)

/**
 * Converts this [UUID] to [Player].
 *
 * @return either a player with certain UUID, or `null` if such a player doesn't exist.
 */
public fun findPlayer(uuid: UUID): Player? = server.getPlayer(uuid)
