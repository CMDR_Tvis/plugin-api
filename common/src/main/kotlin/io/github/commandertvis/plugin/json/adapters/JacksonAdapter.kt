package io.github.commandertvis.plugin.json.adapters

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.io.InputStream
import java.io.OutputStream
import kotlin.reflect.KClass

/**
 * A [JsonAdapter] implementation, which uses [ObjectMapper] for serialization and deserialization.
 */
public open class JacksonAdapter public constructor(
    /**
     * [ObjectMapper] instance, used by this adapter to deserialize and serialize objects.
     */
    protected val mapper: ObjectMapper = jacksonObjectMapper().registerModule(JavaTimeModule()),
) : JsonAdapter {
    public override fun <T : Any> fromJson(stream: InputStream, classOfT: KClass<out T>): T? = try {
        mapper.readValue(stream, classOfT.java)
    } catch (ignored: Exception) {
        null
    }

    override fun toJson(any: Any, stream: OutputStream): Unit =
        mapper.writerWithDefaultPrettyPrinter().writeValue(stream, any)

    public override fun <T : Any> fromJson(string: String, classOfT: KClass<out T>): T? = try {
        mapper.readValue(string, classOfT.java)
    } catch (ignored: Exception) {
        null
    }

    public override fun toJson(any: Any): String? = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(any)

    public companion object : JacksonAdapter()
}
