package io.github.commandertvis.plugin

import io.github.commandertvis.plugin.chatcomponents.ChatComponentsScope
import net.md_5.bungee.api.ChatMessageType
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Creates a [ChatComponentsScope], applies an extension function there and broadcasts it. For example:
 *
 * ```
 * broadcastComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param block the extension function to apply
 */
public inline fun broadcastComponents(block: ChatComponentsScope.() -> Unit): Unit = server.spigot()
    .broadcast(*ChatComponentsScope().apply(block).components.toTypedArray())

/**
 * Creates a [ChatComponentsScope], applies an extension function and sends it to the [CommandSender]. For
 * example:
 *
 * ```
 * player.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * This method may do nothing on old versions (1.8.8) if the sender is not an instance of [Player].
 *
 * @param block the configuring function to apply.
 */
public inline fun CommandSender.replyComponents(block: ChatComponentsScope.() -> Unit): Unit =
    ChatComponentsScope().run {
        block()

        if (javaClass.declaredMethods.any { it.name == "spigot" })
            spigot().sendMessage(*components.toTypedArray())
        else
            (this@replyComponents as? Player)?.spigot()?.sendMessage(*components.toTypedArray())
    }

/**
 * Creates a [ChatComponentsScope], applies an extension function there and sends it to each receiver of this
 * [Iterable]. For example:
 *
 * ```
 * player.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * This method may do nothing on old versions (1.8.8) for senders, which are not instances of [Player].
 *
 * @param block the configuring function to apply.
 */
public inline fun Iterable<CommandSender>.replyComponents(block: ChatComponentsScope.() -> Unit) {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    ChatComponentsScope().run {
        block()
        forEach { it.replyComponents(block) }
    }
}

/**
 * Creates a [ChatComponentsScope], applies an extension function there and sends it to each player of this
 * [Iterable]. For example:
 *
 * ```
 * sender.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param type the type of message.
 * @param block the configuring function to apply.
 */
public inline fun Iterable<Player>.replyComponents(type: ChatMessageType, block: ChatComponentsScope.() -> Unit) {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    ChatComponentsScope().run {
        block()
        forEach { it.spigot().sendMessage(type, *components.toTypedArray()) }
    }
}

/**
 * Creates a [ChatComponentsScope], applies an extension function there and sends it to each receiver of this
 * [Sequence]. For example:
 *
 * ```
 * player.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param block the configuring function to apply.
 */
public inline fun Sequence<CommandSender>.replyComponents(block: ChatComponentsScope.() -> Unit) {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    ChatComponentsScope().run {
        block()
        forEach { it.spigot().sendMessage(*components.toTypedArray()) }
    }
}

/**
 * Creates a [ChatComponentsScope], applies an extension function there and sends it to each player of this
 * [Sequence]. For example:
 *
 * ```
 * sender.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param type the type of message.
 * @param block the configuring function to apply.
 */
public inline fun Sequence<Player>.replyComponents(type: ChatMessageType, block: ChatComponentsScope.() -> Unit) {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    ChatComponentsScope().run {
        block()
        forEach { it.spigot().sendMessage(type, *components.toTypedArray()) }
    }
}

/**
 * Creates a [ChatComponentsScope], applies an extension function. For example:
 *
 * ```
 * sender.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param type the type of message.
 * @param block the configuring function to apply.
 */
public inline fun Player.replyComponents(type: ChatMessageType, block: ChatComponentsScope.() -> Unit) {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    ChatComponentsScope().run {
        block()
        spigot().sendMessage(type, *components.toTypedArray())
    }
}

/**
 * Constructs a [ChatComponentsScope] and applies an extension function there. For example:
 *
 * ```
 * chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param block the configuring function to apply.
 * @return a new [ChatComponentsScope] reference.
 */
public inline fun chatComponents(block: ChatComponentsScope.() -> Unit): ChatComponentsScope {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return ChatComponentsScope().apply(block)
}
