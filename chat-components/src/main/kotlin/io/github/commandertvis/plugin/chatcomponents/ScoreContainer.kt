package io.github.commandertvis.plugin.chatcomponents

import net.md_5.bungee.api.chat.ScoreComponent

/**
 * Chat components DSL [ScoreComponent] wrapper.
 */
@ChatComponentsDslMarker
@JvmInline
public value class ScoreContainer public constructor(public override val component: ScoreComponent) :
    ChatComponentContainer {
    /**
     * Creates [ScoreContainer] by setting [component] as [ScoreComponent] with a name and an objective.
     *
     * @param name the name to [ScoreComponent].
     * @param objective the objective to [ScoreComponent].
     */
    public constructor(name: String, objective: String) : this(ScoreComponent(name, objective))

    /**
     * Appends a subsequence of the specified character sequence to this [ScoreContainer].
     *
     * An invocation of this method of the form `out.append(csq, start, end)` when `csq` is not `null`, behaves in
     * exactly the same way as the invocation.
     *
     * @param csq the character sequence from which a subsequence will be appended.  If `csq` is `null`, then characters
     * will be appended as if `csq` contained the four characters `null`.
     *
     * @param start the index of the first character in the subsequence.
     * @param end the index of the character following the last character in the subsequence.
     * @return a reference to this object.
     */
    public override fun append(csq: CharSequence, start: Int, end: Int): ScoreContainer = apply {
        val s = csq.toString()

        if (start < 0 || start > end || end > s.length)
            throw IndexOutOfBoundsException(
                "start " + start + ", end " + end + ", s.length() "
                        + s.length
            )

        var i = start

        component.let {
            var j = it.value.length

            while (i < end) {
                val arr = it.value.toCharArray()
                arr[j] = s[i]
                it.value = arr.joinToString("")
                i++
                j++
            }
        }
    }

    /**
     * Appends the specified character sequence to this [ScoreContainer].
     *
     * Depending on which class implements the character sequence `csq`, the entire sequence may not be appended
     *
     * @param csq the character sequence to append.  If `csq` is `null`, then the four characters `"null"` are
     * appended to this [ScoreContainer].
     *
     * @return a reference to this object.
     */
    public override fun append(csq: CharSequence): ScoreContainer = apply { component.value += csq }

    /**
     * Appends the specified character to this [ScoreContainer]
     *
     * @param c the character to append
     * @return a reference to this object.
     */
    public override fun append(c: Char): ScoreContainer = apply { component.value += c }

    /**
     * Adds the string to [ScoreComponent.value].
     *
     * [StringBuilder]
     * @receiver the [String] to add.
     * @return a reference to this object.
     */
    public operator fun String.unaryPlus(): ScoreContainer = append(this)
}
