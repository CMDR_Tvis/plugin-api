package io.github.commandertvis.plugin

import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.api.chat.ComponentBuilder
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention
import net.md_5.bungee.api.chat.TextComponent
import java.lang.reflect.Field

internal val ComponentBuilder.currentFieldInternal: Field?
    get() = try {
        javaClass.getDeclaredField("current").also { it.isAccessible = true }
    } catch (ignored: NoSuchFieldException) {
        null
    }

@Suppress("UNCHECKED_CAST")
internal val ComponentBuilder.partsInternal: MutableList<BaseComponent>
    get() = javaClass
        .getDeclaredField("parts")
        .also { it.isAccessible = true }
        .get(this) as MutableList<BaseComponent>

internal var ComponentBuilder.currentInternal: BaseComponent?
    get() = currentFieldInternal?.get(this) as? BaseComponent
    set(value) = run<Unit> { value?.let { currentFieldInternal?.set(this, value) } }

@PublishedApi
internal fun ComponentBuilder.appendInternal(component: BaseComponent): ComponentBuilder =
    if (currentInternal == null) { // modern behaviour
        append(component)
    } else { // 1.8 behaviour (╯°□°）╯︵ ┻━┻
        currentInternal?.let { partsInternal.add(it) }
        val previous = currentInternal

        currentInternal = if (currentFieldInternal?.type?.simpleName == "TextComponent")
            TextComponent(component.duplicate())
        else
            component.duplicate()

        previous?.let {
            currentInternal?.copyFormattingInternal(
                component = it,
                retention = FormatRetention.ALL,
                replace = false
            )
        }

        this
    }

internal fun BaseComponent.copyFormattingInternal(
    component: BaseComponent,
    retention: FormatRetention,
    replace: Boolean
) {
    if (retention == FormatRetention.EVENTS || retention == FormatRetention.ALL) {
        if (replace || clickEvent == null)
            clickEvent = component.clickEvent

        if (replace || hoverEvent == null)
            hoverEvent = component.hoverEvent
    }

    if (retention == FormatRetention.FORMATTING || retention == FormatRetention.ALL) {
        if (replace || color == null)
            color = component.colorRaw

        if (replace || isBoldRaw == null) {
            val ibr: Boolean? = component.isBoldRaw
            setBold(ibr)
        }

        if (replace || isItalicRaw == null) {
            val iir: Boolean? = component.isItalicRaw
            setItalic(iir)
        }

        if (replace || isUnderlinedRaw == null) {
            val iur: Boolean? = component.isUnderlinedRaw
            setUnderlined(iur)
        }

        if (replace || isStrikethroughRaw == null) {
            val isr: Boolean? = component.isStrikethroughRaw
            setStrikethrough(isr)
        }

        if (replace || isObfuscatedRaw == null) {
            val ior: Boolean? = component.isObfuscatedRaw
            setObfuscated(ior)
        }

        if (replace || insertion == null)
            insertion = component.insertion
    }
}
