package io.github.commandertvis.plugin.chatcomponents

import net.md_5.bungee.api.chat.hover.content.Content

/**
 * Stores list of [Content] objects.
 */
@ChatComponentsDslMarker
public class ContentsScope internal constructor(internal val list: MutableList<Content> = mutableListOf()) {
    /**
     * Adds new [Content] object.
     */
    public operator fun Content.unaryPlus() {
        list += this
    }
}
