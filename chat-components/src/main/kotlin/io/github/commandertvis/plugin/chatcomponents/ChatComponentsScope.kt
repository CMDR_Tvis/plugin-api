package io.github.commandertvis.plugin.chatcomponents

import io.github.commandertvis.plugin.appendInternal
import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.api.chat.ComponentBuilder
import org.bukkit.ChatColor
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * The structure of chat components to be built with [ComponentBuilder].
 */
@ChatComponentsDslMarker
public class ChatComponentsScope @PublishedApi internal constructor(
    @PublishedApi internal var builder: ComponentBuilder = ComponentBuilder("")
) {
    /**
     * The components needed to display the message created by this [ChatComponentsScope].
     */
    public inline val components: List<BaseComponent>
        get() = builder.create().asList()

    /**
     * Sets whether the current part is bold.
     *
     * @param value should the chat components be bold or not.
     */
    public fun bold(value: Boolean = true): Unit = run { builder = builder.bold(value) }

    /**
     * Sets the color of the current part.
     *
     * @param value the new color.
     */
    public fun color(value: ChatColor = ChatColor.RESET): Unit = run { builder = builder.color(value.asBungee()) }

    /**
     * Sets whether the current part is italic.
     *
     * @param value should the chat components be italic or not.
     */
    public fun italic(value: Boolean = true): Unit = run { builder = builder.italic(value) }

    /**
     * Sets whether the current part is strikethrough.
     *
     * @param value should the chat components be strikethrough or not.
     */
    public fun strikethrough(value: Boolean = true): Unit = run { builder = builder.strikethrough(value) }

    /**
     * Sets whether the current part is underlined.
     *
     * @param value should the chat components be underlined or not.
     */
    public fun underlined(value: Boolean = true): Unit = run { builder = builder.underlined(value) }

    /**
     * Sets whether the current part is obfuscated.
     *
     * @param value should the chat components be obfuscated or not.
     */
    public fun obfuscated(value: Boolean = true): Unit = run { builder = builder.obfuscated(value) }

    /**
     * Appends new [net.md_5.bungee.api.chat.TextComponent] to this [ChatComponentsScope].
     *
     * @param block the configuring function.
     */
    public inline fun text(block: TextContainer.() -> Unit): Unit = component(initial = TextContainer(), block = block)

    /**
     * Appends new [net.md_5.bungee.api.chat.TranslatableComponent] to this [ChatComponentsScope].
     *
     * @param block the configuring function.
     */
    public inline fun localizedText(block: LocalizedTextContainer.() -> Unit): Unit =
        component(initial = LocalizedTextContainer(), block = block)

    /**
     * Appends new [net.md_5.bungee.api.chat.SelectorComponent] to this [ChatComponentsScope].
     *
     * @param block the configuring function.
     */
    public inline fun selector(block: SelectorContainer.() -> Unit): Unit =
        component(initial = SelectorContainer(), block = block)

    /**
     * Appends new [net.md_5.bungee.api.chat.ScoreComponent] to this [ChatComponentsScope].
     *
     * @param block the configuring function.
     */
    public inline fun score(name: String, objective: String, block: ScoreContainer.() -> Unit): Unit =
        component(initial = ScoreContainer(name, objective), block = block)

    /**
     * Appends new [net.md_5.bungee.api.chat.KeybindComponent] to this [ChatComponentsScope].
     *
     * @param block the configuring function.
     */
    public inline fun keybind(block: KeybindContainer.() -> Unit): Unit =
        component(initial = KeybindContainer(), block = block)

    /**
     * Appends a new chat component to this [ChatComponentsScope].
     *
     * @param block the configuring function.
     */
    public inline fun <reified T : ChatComponentContainer> component(initial: T, block: T.() -> Unit) {
        contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

        initial.run {
            block()
            this@ChatComponentsScope.let { it.builder = it.builder.appendInternal(component) }
        }
    }
}
