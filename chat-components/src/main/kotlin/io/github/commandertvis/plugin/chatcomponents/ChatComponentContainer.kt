package io.github.commandertvis.plugin.chatcomponents

import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.HoverEvent
import org.bukkit.ChatColor

/**
 * This type describes DSL tag of [BaseComponent].
 */
public interface ChatComponentContainer : Appendable {
    /**
     * The component, edited by this [ChatComponentContainer].
     */
    public val component: BaseComponent

    /**
     * Changes whether this component is bold.
     *
     * @param value the new state if to make this component bold or not.
     */
    public fun bold(value: Boolean = true): Unit = run { component.isBold = value }

    /**
     * Changes the color of the component.
     *
     * @param value the new color.
     */
    public fun color(value: ChatColor = ChatColor.RESET): Unit = run { component.color = value.asBungee() }

    /**
     * Changes whether this component is italic.
     *
     * @param value the new state if to make this component italic or not.
     */
    public fun italic(value: Boolean = true): Unit = run { component.isItalic = value }

    /**
     * Changes this component's click action.
     *
     * @param action the type of action.
     * @param value the value of [action].
     */
    public fun onClick(action: ClickEvent.Action, value: String) {
        component.clickEvent = ClickEvent(action, value)
    }

    /**
     * Changes this component's click action.
     *
     * @param action the type of action.
     * @param value the supplier lambda of the value of [action].
     */
    public fun onClick(action: ClickEvent.Action, value: () -> String) {
        component.clickEvent = ClickEvent(action, value())
    }

    /**
     * Changes this component's hover action.
     *
     * @param action the type of action.
     * @param value the value of [action].
     */
    @Suppress("DEPRECATION")
    @Deprecated("Use ContentsScope", ReplaceWith("onHoverWithContents"))
    public fun onHover(action: HoverEvent.Action, value: ChatComponentsScope) {
        component.hoverEvent = HoverEvent(action, value.builder.create())
    }

    /**
     * Changes this component's hover action. The value of [action] is specified by a [ChatComponentsScope] that's
     * configured by an extension function.
     *
     * @param action the type of action.
     * @param block the extension function to apply.
     */
    @Suppress("DEPRECATION")
    @Deprecated("Use ContentsScope", ReplaceWith("onHoverWithContents"))
    public fun onHover(action: HoverEvent.Action, block: ChatComponentsScope.() -> Unit) {
        component.hoverEvent = HoverEvent(action, ChatComponentsScope().apply(block).components.toTypedArray())
    }

    /**
     * Changes this component's hover action.
     *
     * @param action the type of action.
     * @param block the extension function to apply to [ContentsScope] used to provide hover tip contents.
     */
    public fun onHoverWithContents(action: HoverEvent.Action, block: ContentsScope.() -> Unit) {
        component.hoverEvent = HoverEvent(action, ContentsScope().apply(block).list)
    }

    /**
     * Changes whether this component is strikethrough.
     *
     * @param value the new state if to make this component strikethrough or not.
     */
    public fun strikethrough(value: Boolean = true): Unit = run { component.isStrikethrough = value }

    /**
     * Changes whether this component is underlined.
     *
     * @param value the new state if to make this component underlined or not.
     */
    public fun underlined(value: Boolean = true): Unit = run { component.isUnderlined = value }

    /**
     * Changes whether this component is obfuscated.
     *
     * @param value the new state if to make this component obfuscated or not.
     */
    public fun obfuscated(value: Boolean = true): Unit = run { component.isObfuscated = value }
}
