package io.github.commandertvis.plugin.chatcomponents

@DslMarker
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS)
internal annotation class ChatComponentsDslMarker
