package io.github.commandertvis.plugin.chatcomponents

import net.md_5.bungee.api.chat.TextComponent

/**
 * Chat components DSL [TextComponent] wrapper.
 */
@ChatComponentsDslMarker
@JvmInline
public value class TextContainer public constructor(public override val component: TextComponent = TextComponent("")) :
    ChatComponentContainer {

    /**
     * Appends the specified character sequence to this [TextContainer].
     *
     * Depending on which class implements the character sequence `csq`, the entire sequence may not be appended.
     *
     * @param csq the character sequence to append. If `csq` is `null`, then the four characters `"null"` are
     * appended to this [TextContainer]
     * @return a reference to this [TextContainer]
     */
    public override fun append(csq: CharSequence): TextContainer = apply { component.text += csq }

    /**
     * Appends a subsequence of the specified character sequence to this [TextContainer].
     *
     * An invocation of this method of the form `out.append(csq, start, end)` when `csq` is not `null`, behaves in
     * exactly the same way as the invocation.
     *
     * @param csq the character sequence from which a subsequence will be appended.  If `csq` is `null`, then characters
     * will be appended as if `csq` contained the four characters `null`.
     *
     * @param start the index of the first character in the subsequence.
     * @param end the index of the character following the last character in the subsequence.
     * @return a reference to this object.
     */
    public override fun append(csq: CharSequence, start: Int, end: Int): TextContainer = apply {
        val s = csq.toString()

        if (start < 0 || start > end || end > s.length)
            throw IndexOutOfBoundsException(
                "start " + start + ", end " + end + ", s.length() "
                        + s.length
            )

        var i = start

        component.let {
            var j = it.text.length

            while (i < end) {
                val arr = it.text.toCharArray()
                arr[j] = s[i]
                it.text = arr.joinToString("")
                i++
                j++
            }
        }
    }

    /**
     * Appends the specified character to this [LocalizedTextContainer]
     *
     * @param c the character to append
     * @return a reference to this [LocalizedTextContainer]
     */
    public override fun append(c: Char): TextContainer = apply { component.text += c }

    /**
     * Adds the string to [TextComponent.text]
     */
    public operator fun String.unaryPlus(): TextContainer = append(this)
}
