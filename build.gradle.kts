@file:Suppress("UNUSED_VARIABLE")

import org.jetbrains.dokka.gradle.*
import java.net.*

plugins {
    `maven-publish`
    alias(libs.plugins.dokka)
    alias(libs.plugins.kotlin.jvm)
}

allprojects {
    apply(plugin = "kotlin")

    kotlin {
        explicitApi()

        sourceSets.all {
            with(languageSettings) {
                progressiveMode = true
                optIn("kotlin.contracts.ExperimentalContracts")
                optIn("kotlin.reflect.jvm.ExperimentalReflectionOnLambdas")
            }
        }
    }

    description = "Versatile and useful Spigot plugin utilities in Kotlin"
    group = "io.github.commandertvis.plugin"
    version = "18.0.0"

    repositories {
        mavenCentral()
        maven(url = "https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
        maven(url = "https://oss.sonatype.org/content/groups/public/")
    }

    dependencies {
        api(rootProject.libs.jackson.dataformat.yaml) { exclude("org.yaml", "snakeyaml") }
        api(rootProject.libs.jackson.datatype.jsr310)
        api(rootProject.libs.jackson.module.kotlin) { exclude("org.jetbrains.kotlin") }
        api(rootProject.libs.kotlin.stdlib.jdk8)
        api(rootProject.libs.kotlin.reflect)
        api(rootProject.libs.snakeyaml)
        compileOnly(rootProject.libs.spigot.api) { exclude(group = "org.yaml", module = "snakeyaml") }
    }

    tasks.compileKotlin.get().kotlinOptions.jvmTarget = "1.8"
}

subprojects {
    val p = this@subprojects
    apply<MavenPublishPlugin>()
    apply<DokkaPlugin>()

    tasks {
        val sourcesJar by creating(Jar::class) {
            from(sourceSets.main.get().allSource)
            archiveClassifier.set("sources")
        }

        dokkaJavadoc.get().outputDirectory.set(javadoc.get().destinationDir)

        withType<DokkaTaskPartial> {
            dokkaSourceSets.configureEach {
                includes.from(rootProject.file("PACKAGES.md"))
                val readmeFile = file("README.md")
                if (readmeFile.exists()) includes.from(readmeFile)
                externalDocumentationLink("https://fasterxml.github.io/jackson-databind/javadoc/2.13/")
                externalDocumentationLink("https://fasterxml.github.io/jackson-dataformats-text/javadoc/yaml/2.13/")
                externalDocumentationLink("https://static.javadoc.io/com.google.code.gson/gson/2.8.5/")

                externalDocumentationLink(
                    "https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/",
                    "https://kotlin.github.io/kotlinx.coroutines/package-list",
                )

                sourceLink {
                    localDirectory.set(file("src/main/kotlin"))

                    remoteUrl.set(
                        URL("https://gitlab.com/CMDR_Tvis/${rootProject.name}/tree/master/${p.name}/src/main/kotlin/")
                    )
                }
            }
        }

        val dokkaJar by creating(Jar::class) {
            dependsOn(dokkaJavadoc)
            group = JavaBasePlugin.DOCUMENTATION_GROUP
            archiveClassifier.set("javadoc")
            from(javadoc.get().destinationDir)
        }

        publish.get().dependsOn(build)

        publishing {
            publications {
                val mavenJava by creating(MavenPublication::class) {
                    from(p.components["java"])

                    artifacts {
                        artifact(dokkaJar)
                        artifact(sourcesJar)
                    }

                    pom {
                        description.set(rootProject.description)
                        url.set("https://gitlab.com/CMDR_Tvis/${rootProject.name}")
                        inceptionYear.set("2021")

                        developers {
                            developer {
                                email.set("postovalovya@gmail.com")
                                id.set("CMDR_Tvis")
                                name.set("Iaroslav Postovalov")
                                roles.set(listOf("architect", "developer"))
                                timezone.set("7")
                                url.set("https://gitlab.com/CMDR_Tvis")
                            }
                        }

                        licenses {
                            license {
                                comments.set("Open-source license")
                                distribution.set("repo")
                                name.set("MIT License")
                                url.set("https://gitlab.com/CMDR_Tvis/${rootProject.name}/blob/master/LICENSE")
                            }
                        }

                        scm {
                            connection.set("scm:git:git@gitlab.com:CMDR_Tvis/${rootProject.name}.git")
                            developerConnection.set("scm:git:git@gitlab.com:CMDR_Tvis/${rootProject.name}.git")
                            url.set("git@gitlab.com:CMDR_Tvis/${rootProject.name}.git")
                        }
                    }
                }
            }

            repositories.maven(url = "https://gitlab.com/api/v4/projects/10077943/packages/maven") {
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }

                authentication { val header by registering(HttpHeaderAuthentication::class) }
            }
        }
    }
}

tasks.dokkaHtmlMultiModule.get().outputDirectory.set(file("public/"))
