package io.github.commandertvis.plugin.command.contexts

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Float] type.
 */
public object FloatContext : OneStringContext<Float>() {
    public override fun resolve(string: String): Float? = string.toFloatOrNull()
}
