package io.github.commandertvis.plugin.command.contexts

import io.github.commandertvis.plugin.findPluginCommand
import org.bukkit.command.PluginCommand

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [PluginCommand] type.
 */
public object PluginCommandContext : OneStringContext<PluginCommand>() {
    public override fun resolve(string: String): PluginCommand? = findPluginCommand(string)
}
