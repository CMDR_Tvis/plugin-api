package io.github.commandertvis.plugin.command.dsl

import io.github.commandertvis.plugin.command.FunctionNWithContexts
import io.github.commandertvis.plugin.command.addContexts
import org.bukkit.Location
import org.bukkit.command.CommandSender
import kotlin.jvm.functions.FunctionN

/**
 * Represents builder scope of a simple subcommand with a description, an action and tab completer.
 */
@CommandDslMarker
public class SubcommandScope @PublishedApi internal constructor() {
    /**
     * This subcommand's action.
     */
    public var action: FunctionNWithContexts<Unit>? = null

    /**
     * This subcommand's tab completer action.
     */
    public var tabAction: (CommandSender.(alias: String, location: Location?, args: List<String>) -> List<String>)? =
        null

    /**
     * Sets this subcommand's action.
     */
    public fun action(response: FunctionN<Unit>): Unit = run { action = response.addContexts() }

    /**
     * Sets this subcommand's action.
     */
    public fun action(response: Function<Unit>): Unit = run { action = response.addContexts() }

    /**
     * Sets this subcommand's tab completer action.
     */
    public fun tabAction(
        response: CommandSender.(alias: String, location: Location?, args: List<String>) -> List<String>
    ): Unit = run { tabAction = response }
}
