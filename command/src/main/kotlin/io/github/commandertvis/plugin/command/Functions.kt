package io.github.commandertvis.plugin.command

import io.github.commandertvis.plugin.command.contexts.CommandContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import java.lang.reflect.Method
import java.util.concurrent.ConcurrentHashMap
import kotlin.jvm.functions.FunctionN
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.valueParameters
import kotlin.reflect.jvm.reflect

private val contextsCache = ConcurrentHashMap<KClass<out CommandContext<*>>, CommandContext<*>>()

private fun instantiateContext(klass: KClass<out CommandContext<*>>): CommandContext<*> {
    if (contextsCache.containsKey(klass))
        return contextsCache.getValue(klass)

    klass.objectInstance?.let {
        contextsCache[klass] = it
        return it
    }

    klass.createInstance().let {
        contextsCache[klass] = it
        return it
    }
}

private fun Function<*>.getContexts(): List<CommandContext<*>>? {
    return (javaClass.methods
        .filter { it.name == "invoke" }
        .map(Method::getAnnotations)
        .flatMap(Array<Annotation>::asIterable)
        .filterIsInstance<Contexts>()
        .map(Contexts::types)
        .singleOrNull() ?: return null)
        .map(::instantiateContext)
}

@Suppress("UNCHECKED_CAST")
internal fun <R> Function<R>.addContexts(): FunctionNWithContexts<R> {
    if (this is FunctionNWithContexts<R>) return this
    val contexts = getContexts()

    if (this is FunctionN<R>) return object : FunctionNWithContexts<R> {
        override val contexts: List<CommandContext<*>> = contexts.orEmpty()

        override val arity: Int
            get() = this@addContexts.arity

        override fun invoke(vararg args: Any?): R = this@addContexts.invoke(args)
    }

    val k = checkNotNull(reflect()) { "Can't reflect function $this." }

    return object : FunctionNWithContexts<R> {
        override val arity: Int
            get() = k.valueParameters.size

        override val contexts: List<CommandContext<*>> = contexts.orEmpty()

        override fun invoke(vararg args: Any?): R = k.call(this@addContexts, *args)
    }
}
