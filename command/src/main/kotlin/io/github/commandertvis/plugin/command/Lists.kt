package io.github.commandertvis.plugin.command

import io.github.commandertvis.plugin.command.contexts.CommandContext
import io.github.commandertvis.plugin.command.contexts.DefaultContext

/**
 * Parses this [List] of [String] to [List] of objects, deserialized iteratively by given command contexts.
 *
 * @param contexts the contexts that deserialize strings.
 * @return list of data, parsed of strings by contexts.
 */
public fun List<String>.parseArguments(vararg contexts: CommandContext<out Any>): List<Any?> =
    parseArguments(contexts.asIterable())

/**
 * Parses this [List] of [String] to [List] of objects, deserialized iteratively by given command contexts.
 *
 * @param contexts the contexts that deserialize strings.
 * @return list of data, parsed of strings by contexts.
 */
public fun List<String>.parseArguments(contexts: Iterable<CommandContext<out Any>>): List<Any?> {
    if (contexts.firstOrNull() == DefaultContext)
        return listOf(this)

    var parsed = 0
    val data = mutableListOf<Any?>()

    contexts.forEach {
        val newParsed = parsed + it.length

        data += try {
            it.resolve(strings = subList(parsed, newParsed).toTypedArray())
        } catch (ignored: Exception) {
            null
        }

        parsed = newParsed
    }

    return data
}
