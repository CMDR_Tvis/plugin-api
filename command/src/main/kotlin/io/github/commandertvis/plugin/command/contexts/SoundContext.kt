package io.github.commandertvis.plugin.command.contexts

import io.github.commandertvis.plugin.enumValueOrNullOf
import org.bukkit.Sound

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Sound] type.
 */
public object SoundContext : OneStringContext<Sound>() {
    public override fun resolve(string: String): Sound? = enumValueOrNullOf<Sound>(string)
}
