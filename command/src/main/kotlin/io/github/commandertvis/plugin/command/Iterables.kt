package io.github.commandertvis.plugin.command

internal fun <E> Iterable<Iterable<E>>.linearizeToHashSet(): Set<E> {
    val destination = hashSetOf<E>()
    forEach { destination += it }
    return destination
}
