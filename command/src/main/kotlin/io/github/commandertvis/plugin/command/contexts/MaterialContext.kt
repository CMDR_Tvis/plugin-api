package io.github.commandertvis.plugin.command.contexts

import io.github.commandertvis.plugin.findMaterialByKey
import org.bukkit.Material

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Material] type.
 */
public object MaterialContext : OneStringContext<Material>() {
    public override fun resolve(string: String): Material? = findMaterialByKey(string)
}
