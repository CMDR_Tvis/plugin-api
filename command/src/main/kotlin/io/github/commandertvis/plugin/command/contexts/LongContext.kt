package io.github.commandertvis.plugin.command.contexts

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Long] type.
 */
public object LongContext : OneStringContext<Long>() {
    public override fun resolve(string: String): Long? = string.toLongOrNull()
}
