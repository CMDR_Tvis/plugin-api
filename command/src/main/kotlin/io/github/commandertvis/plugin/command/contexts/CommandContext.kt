package io.github.commandertvis.plugin.command.contexts

/**
 * Represents a command context - a structure, capable to deserialize an [Array] of [String] to a certain type.
 *
 * @param T the type, that this [CommandContext] deserializes.
 */
public interface CommandContext<T : Any> {
    /**
     * The size of [Array], that given to this [CommandContext].
     */
    public val length: Int

    /**
     * Tries to parse the transferred [Array] into new [T].
     *
     * @param strings the input data, an [Array] with [Array.size] of at least of [length]
     * @return either the new [T] instance or `null`.
     */
    public fun resolve(strings: Array<String>): T?
}
