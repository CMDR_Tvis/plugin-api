package io.github.commandertvis.plugin.command.dsl

/**
 * Represents the state of action calling in [ExecutionScope].
 */
@CommandDslMarker
public class CommandPipelineContext internal constructor() {
    @PublishedApi
    internal var isCancelled: Boolean = false

    /**
     * Interrupts the command's action.
     */
    public fun cancel(): Unit = run { isCancelled = true }
}
