package io.github.commandertvis.plugin.command.contexts

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Char] type.
 */
public object CharContext : OneStringContext<Char>() {
    public override fun resolve(string: String): Char? = if (string.length == 1)
        string[0]
    else
        null
}
