package io.github.commandertvis.plugin.command.contexts

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Double] type.
 */
public object DoubleContext : OneStringContext<Double>() {
    public override fun resolve(string: String): Double? = string.toDoubleOrNull()
}
