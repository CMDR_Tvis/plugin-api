package io.github.commandertvis.plugin.command.contexts

import io.github.commandertvis.plugin.findPlayer
import io.github.commandertvis.plugin.toUuidOrNull
import org.bukkit.entity.Player

/**
 * The [CommandContext] implementation for [Player] type.
 */
public object PlayerContext : OneStringContext<Player>() {
    /**
     * Finds a [Player] by their UUID or name.
     *
     * @param string the name or UUID of the player.
     * @return a Player with certain UUID or player or `null`.
     */
    public override fun resolve(string: String): Player? {
        findPlayer(string)?.let { return it }
        findPlayer(string.toUuidOrNull() ?: return null)?.let { return it }
        return null
    }
}
