package io.github.commandertvis.plugin.command.dsl

import io.github.commandertvis.plugin.command.*
import io.github.commandertvis.plugin.command.FunctionNWithContexts
import io.github.commandertvis.plugin.command.linearizeToHashSet
import io.github.commandertvis.plugin.command.addContexts
import org.bukkit.Location
import org.bukkit.command.CommandSender
import kotlin.jvm.functions.FunctionN

/**
 * Represents a builder scope of command's execution action.
 */
@CommandDslMarker
public open class ExecutionScope @PublishedApi internal constructor() {
    @PublishedApi
    internal open var commandInternal: CommandScope? = null

    @PublishedApi
    internal var defaultInternal: SubcommandScope? = null

    @PublishedApi
    internal var executionInterceptorInternal:
            (CommandPipelineContext.(sender: CommandSender, alias: String, args: List<String>) -> Unit)? = null

    /**
     * The command where this [ExecutionScope] is used.
     */
    public inline val command: CommandScope?
        get() = commandInternal

    /**
     * Runs before each calling of the action of this [ExecutionScope].
     */
    public inline val executionInterceptor:
            (CommandPipelineContext.(sender: CommandSender, alias: String, args: List<String>) -> Unit)?
        get() = executionInterceptorInternal

    /**
     * Already registered command reactions of this branch.
     */
    public val actions: MutableMap<Set<String>, FunctionNWithContexts<Unit>> = hashMapOf()

    /**
     * The branches that are invoked from this one.
     */
    public val children: MutableMap<Set<String>, ExecutionScope> = hashMapOf()

    /**
     * Already registered tab completers of this branch.
     */
    public val tabActions: MutableMap<
            Set<String>, CommandSender.(alias: String, location: Location?, args: List<String>) -> List<String>> =
        hashMapOf()

    /**
     * The default subcommand, invoked in this branch.
     */
    public inline val default: SubcommandScope?
        get() = defaultInternal

    /**
     * Changes the handler before command executes.
     */
    public fun onCommandExecution(
        handler: (CommandPipelineContext.(sender: CommandSender, alias: String, args: List<String>) -> Unit)
    ): Unit = run { executionInterceptorInternal = handler }

    /**
     * Adds a new subcommand branch with own features.
     *
     * For example:
     *
     * ```
     * setOf("mySubcommand1", "mySubcommand2") routes { default { _, _, _ -> doWork() } }
     * ```
     *
     * @param block the applied to the new [ExecutionScope] data
     */
    public inline infix fun Set<String>.routes(block: ExecutionScope.() -> Unit): Unit = ExecutionScope().let {
        it.block()
        it.commandInternal = command
        children[this] = it
    }

    internal fun execute(sender: CommandSender, alias: String, args: List<String>) {
        val call = CommandPipelineContext()
        executionInterceptorInternal?.invoke(call, sender, alias, args)

        if (call.isCancelled)
            return

        children.forEach { (aliases, scope) ->
            if (args.firstOrNull() in aliases) {
                scope.execute(sender, args.first(), args.drop(1))
                return
            }
        }

        actions.forEach { (aliases, action) ->
            if (args.firstOrNull() in aliases) {
                action(
                    sender,
                    args.first(),
                    *args.drop(1).parseArguments(action.contexts).toTypedArray()
                )

                return
            }
        }

        default?.let { d ->
            d.action?.let { a ->
                a.invoke(
                    sender,
                    alias,
                    *args.parseArguments(a.contexts).toTypedArray()
                )
            }
        }
    }

    internal fun tabComplete(
        sender: CommandSender,
        alias: String,
        location: Location?,
        args: List<String>
    ): List<String> {
        val argsWithoutFirst = args.drop(1)

        children.forEach {
            if (args.firstOrNull() in it.key)
                return it.value.tabComplete(sender, args.first(), location, argsWithoutFirst)
        }

        tabActions.forEach {
            if (args.firstOrNull() in it.key)
                return it.value.invoke(sender, args.first(), location, argsWithoutFirst)
        }

        return hashSetOf<String>()
            .apply {
                this += children.keys.linearizeToHashSet()
                this += tabActions.keys.linearizeToHashSet()
                defaultInternal?.tabAction?.invoke(sender, alias, location, args)?.let { this += it }
            }

            .filter { args.firstOrNull()?.let { first -> it.startsWith(first) } ?: true }
    }

    /**
     * Specifies a complex reaction on a tab complete.
     *
     * For example:
     *
     * ```
     * setOf("anAction1", "anAction2") respondsTab { _, _, args -> args.map(String::toUpperCase) }
     * ```
     *
     * @param block the action of the tab completer.
     */
    public infix fun Set<String>.respondsTab(
        block: CommandSender.(alias: String, location: Location?, args: List<String>) -> List<String>
    ): Unit = run { tabActions[this] = block }

    /**
     * Sets an action, when this group of strings invoked.
     *
     * For example:
     *
     * ```
     * setOf("receiveHelloWorld", "helloWorld", "hW") responds { _, _ -> sendMessage("&a&lHello, world".color()) }
     * ```
     *
     * @param block the action of this subcommand.
     */
    public infix fun Set<String>.responds(block: FunctionN<Unit>): Unit = run { actions[this] = block.addContexts() }

    /**
     * Sets an action, when this group of strings invoked.
     *
     * For example:
     *
     * ```
     * setOf("receiveHelloWorld", "helloWorld", "hW") responds { _, _ -> sendMessage("&a&lHello, world".color()) }
     * ```
     *
     * @param block the action of this subcommand.
     */
    public infix fun Set<String>.responds(block: Function<Unit>): Unit = run { actions[this] = block.addContexts() }

    /**
     * Sets a default action, when this execution is called.
     *
     * @param block the new default action.
     */
    public inline fun default(block: SubcommandScope.() -> Unit): Unit =
        run { defaultInternal = SubcommandScope().apply(block) }

    /**
     * Sets an action, when this string invoked.
     *
     * For example:
     *
     * ```
     * "receiveHelloWorld" responds { _, _ -> sendMessage("&a&lHello, world".color()) }
     * ```
     *
     * @param block the action of this subcommand.
     */
    public infix fun String.responds(block: FunctionN<Unit>): Unit =
        setOf(this) responds block

    /**
     * Sets an action, when this string invoked.
     *
     * For example:
     *
     * ```
     * "receiveHelloWorld" responds { _, _ -> sendMessage("&a&lHello, world".color()) }
     * ```
     *
     * @param block the action of this subcommand.
     */
    public infix fun String.responds(block: Function<Unit>): Unit =
        setOf(this) responds block.addContexts()

    /**
     * Specifies more complex reaction on a tab complete.
     *
     * For example:
     *
     * ```
     * "anAction" respondsTab { _, _, args -> args.map(String::toUpperCase) }
     * ```
     *
     * @param block the action of the tab completer.
     */
    public infix fun String.respondsTab(
        block: CommandSender.(alias: String, location: Location?, args: List<String>) -> List<String>
    ): Unit =
        setOf(this) respondsTab block

    /**
     * Adds a new subcommand branch with own features.
     *
     * For example:
     *
     * ```
     * "mySubcommand" routes { default { _, _, _ -> doWork() } }
     * ```
     *
     * @param block the applied to the new [ExecutionScope] data.
     */
    public infix fun String.routes(block: ExecutionScope.() -> Unit): Unit = setOf(this) routes block

    /**
     * Adds a new subcommand of this branch.
     *
     * @param block the applied to the new [SubcommandScope] data.
     * @return the new [SubcommandScope] reference.
     */
    public infix fun String.subcommand(block: SubcommandScope.() -> Unit): SubcommandScope =
        setOf(this) subcommand block

    /**
     * Adds a new subcommand of this branch.
     *
     * @param block the applied to the new [SubcommandScope] data.
     * @return new [SubcommandScope] reference.
     */
    public inline infix fun Set<String>.subcommand(block: SubcommandScope.() -> Unit): SubcommandScope =
        SubcommandScope().also {
            it.block()
            run { this responds (it.action ?: return@run) }
            run { this respondsTab (it.tabAction ?: return@run) }
        }
}
