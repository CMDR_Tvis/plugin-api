package io.github.commandertvis.plugin.command.contexts

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Short] type.
 */
public object ShortContext : OneStringContext<Short>() {
    public override fun resolve(string: String): Short? = string.toShortOrNull()
}
