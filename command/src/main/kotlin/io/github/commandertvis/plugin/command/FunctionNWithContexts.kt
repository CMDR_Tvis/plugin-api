package io.github.commandertvis.plugin.command

import io.github.commandertvis.plugin.command.contexts.CommandContext
import kotlin.jvm.functions.FunctionN

/**
 * Represents function that stores list of [CommandContext] to parse strings into arguments of this function.
 *
 * @param R the return type.
 */
public interface FunctionNWithContexts<R> : FunctionN<R> {
    /**
     * The contexts list.
     */
    public val contexts: List<CommandContext<*>>
}
