package io.github.commandertvis.plugin.command.dsl

import io.github.commandertvis.plugin.command.ManagedCommand
import io.github.commandertvis.plugin.server
import org.bukkit.command.CommandSender
import java.util.logging.Level

/**
 * Represents a builder scope of a game command.
 *
 * @throws IllegalArgumentException when the set of aliases is empty.
 */
@CommandDslMarker
public class CommandScope @PublishedApi internal constructor(
    /**
     * This command's fallback prefix.
     */
    public val prefix: String,
    /**
     * This command's calling aliases.
     */
    public val aliases: Set<String>
) : ExecutionScope() {
    override var commandInternal: CommandScope?
        get() = this
        set(_) = error("Cannot replace the root ExecutionScope")

    /**
     * The command's description.
     */
    public var description: String? = null

    /**
     * The action, executed, when exception occurs during command execution or tab completing.
     */
    public var exceptionHandler:
            ((sender: CommandSender, args: List<String>, alias: String, throwable: Throwable) -> Unit) =
        { sender, args, alias, throwable ->
            server.logger.log(
                Level.SEVERE,
                throwable
            ) { "Exception at command '{$aliases}/.../$alias $args', called by $sender" }
        }

    init {
        require(aliases.isNotEmpty()) { "command needs at least one alias" }
    }

    /**
     * Sets the action, executed, when exception occurs during command execution or tab completing.
     *
     * @param block the new action.
     */
    public fun exceptionHandler(
        block: (sender: CommandSender, args: List<String>, alias: String, throwable: Throwable) -> Unit
    ): Unit = run { exceptionHandler = block }

    @PublishedApi
    internal fun injectToBukkit(): ManagedCommand = ManagedCommand(this)

    internal fun addResultExceptionHandler(
        result: Result<*>,
        sender: CommandSender,
        alias: String,
        args: List<String>
    ) {
        result.onFailure { exceptionHandler.invoke(sender, args, alias, it) }
    }
}
