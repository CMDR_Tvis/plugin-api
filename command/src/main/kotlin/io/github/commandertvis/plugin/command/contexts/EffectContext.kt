package io.github.commandertvis.plugin.command.contexts

import io.github.commandertvis.plugin.enumValueOrNullOf
import org.bukkit.Effect

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Effect] type.
 */
public object EffectContext : OneStringContext<Effect>() {
    public override fun resolve(string: String): Effect? = enumValueOrNullOf<Effect>(string)
}
