package io.github.commandertvis.plugin.command

import io.github.commandertvis.plugin.command.dsl.CommandScope
import io.github.commandertvis.plugin.server
import org.bukkit.Location
import org.bukkit.Server
import org.bukkit.command.Command
import org.bukkit.command.CommandMap
import org.bukkit.command.CommandSender
import kotlin.reflect.KProperty1
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.isAccessible

/**
 * Represents a [Command] that uses [CommandScope] as source of its actions and information.
 */
@Suppress("UNCHECKED_CAST")
public class ManagedCommand @PublishedApi internal constructor(private val scope: CommandScope) :
    Command(scope.aliases.first()) {

    init {
        scope.description?.let { description = it }
        aliases = scope.aliases.toMutableList()

        (server.javaClass
            .kotlin
            .declaredMemberProperties
            .find { it.name == "commandMap" } as? KProperty1<Server, CommandMap>)?.let {
            it.isAccessible = true
            it.get(server).register(scope.prefix, this)
        }
    }

    /**
     * Executes the command, returning its success.
     *
     * @param sender source object, which is executing this command.
     * @param commandLabel the alias of the command used.
     * @param args all arguments passed to the command, split via ' '.
     * @return `true`.
     */
    public override fun execute(
        sender: CommandSender,
        commandLabel: String,
        args: Array<String>
    ): Boolean {
        scope.addResultExceptionHandler(
            runCatching { scope.execute(sender, commandLabel, args.toMutableList()) },
            sender,
            commandLabel,
            args.asList()
        )

        return true
    }

    /**
     * Executed on tab completion for this command, returning a list of options the player can tab through.
     *
     * @param sender source object, which is executing this command.
     * @param alias the alias being used.
     * @param args all arguments passed to the command, split via ' '.
     * @return a list of tab-completions for the specified arguments.
     */
    public override fun tabComplete(sender: CommandSender, alias: String, args: Array<String>): List<String> =
        tabComplete(sender, alias, args, null)

    /**
     * Executed on tab completion for this command, returning a list of options the player can tab through.
     *
     * @param sender source object, which is executing this command.
     * @param alias the alias being used.
     * @param args all arguments passed to the command, split via ' '.
     * @param location the position looked at by the sender, or `null` if none.
     * @return a list of tab-completions for the specified arguments.
     */
    public override fun tabComplete(
        sender: CommandSender,
        alias: String,
        args: Array<String>,
        location: Location?
    ): List<String> {
        scope.let {
            it.addResultExceptionHandler(
                runCatching { return it.tabComplete(sender, alias, location, args.asList()) },
                sender,
                alias,
                args.asList()
            )
        }

        return emptyList()
    }
}
