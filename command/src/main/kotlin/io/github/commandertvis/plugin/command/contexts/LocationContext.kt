package io.github.commandertvis.plugin.command.contexts

import io.github.commandertvis.plugin.findWorld
import org.bukkit.Location

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Location] type.
 */
public object LocationContext : CommandContext<Location> {
    public override val length: Int
        get() = 4

    public override fun resolve(strings: Array<String>): Location? {
        val x = strings[1].toDoubleOrNull()
        val y = strings[2].toDoubleOrNull()
        val z = strings[3].toDoubleOrNull()

        if (x != null && y != null && z != null)
            return Location(findWorld(strings[0]), x, y, z)

        return null
    }
}
