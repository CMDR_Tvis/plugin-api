package io.github.commandertvis.plugin.command.contexts

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [Boolean] type.
 */
public object BooleanContext : OneStringContext<Boolean>() {
    public override fun resolve(string: String): Boolean? = when (string) {
        "true" -> true
        "false" -> false
        else -> null
    }
}
