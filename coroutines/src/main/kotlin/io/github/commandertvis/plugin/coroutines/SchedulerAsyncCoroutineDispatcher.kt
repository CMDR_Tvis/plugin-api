package io.github.commandertvis.plugin.coroutines

import io.github.commandertvis.plugin.server
import kotlinx.coroutines.CoroutineDispatcher
import org.bukkit.plugin.Plugin
import kotlin.coroutines.CoroutineContext

// Based on https://github.com/Shynixn/MCCoroutine.

internal class SchedulerAsyncCoroutineDispatcher(private val plugin: Plugin) : CoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) {
        if (server.isPrimaryThread) plugin.server.scheduler.runTaskAsynchronously(plugin, block) else block.run()
    }
}
