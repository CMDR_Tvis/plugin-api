package io.github.commandertvis.plugin.coroutines

import io.github.commandertvis.plugin.server
import kotlinx.coroutines.CoroutineDispatcher
import org.bukkit.plugin.Plugin
import kotlin.coroutines.CoroutineContext

// Based on https://github.com/Shynixn/MCCoroutine.

internal class SchedulerPrimaryCoroutineDispatcher(private val plugin: Plugin) : CoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) {
        if (server.isPrimaryThread) block.run() else plugin.server.scheduler.runTask(plugin, block)
    }
}
