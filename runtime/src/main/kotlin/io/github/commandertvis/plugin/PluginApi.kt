package io.github.commandertvis.plugin

import io.github.commandertvis.plugin.gui.setupRuntimeForGui
import org.bukkit.plugin.java.JavaPlugin

internal class PluginApi public constructor() : JavaPlugin() {
    public override fun onEnable(): Unit = setupRuntimeForGui()
}
