plugins { alias(libs.plugins.shadow) }
base.archivesName.set("plugin-api")

dependencies {
    api(projects.common)
    api(projects.coroutines)
    api(projects.chatComponents)
    api(projects.command)
    api(projects.gui)
}

tasks {
    jar.get().archiveClassifier.set("default")

    shadowJar {
        archiveClassifier.set(null as String?)
        relocate("org.yaml.snakeyaml", "io.github.commandertvis.plugin.internal.org.yaml.snakeyaml")
    }

    artifacts.archives(shadowJar)

    processResources.get().expand(
        "pluginName" to "PluginApi",
        "rootName" to parent?.name,
        "description" to description,
        "version" to version,
    )
}
