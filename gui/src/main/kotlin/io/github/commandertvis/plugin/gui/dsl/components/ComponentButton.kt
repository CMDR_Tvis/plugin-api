package io.github.commandertvis.plugin.gui.dsl.components

import io.github.commandertvis.plugin.gui.GuiLocation
import io.github.commandertvis.plugin.gui.ItemHolder
import io.github.commandertvis.plugin.gui.dsl.GUI
import io.github.commandertvis.plugin.gui.dsl.GuiDslMarker
import io.github.commandertvis.plugin.gui.dsl.GuiView
import org.bukkit.inventory.ItemStack

/**
 * Represents a button component for [GUI]. It reacts to any click to execute its action.
 *
 * @param S the type of session of GUIs, dedicated to this button.
 */
@GuiDslMarker
public open class ComponentButton<S> public constructor(
    /**
     * The [ItemStack] that represents this button
     */
    item: ItemStack,

    /**
     * The action function of this button.
     */
    public var action: (GuiView<S>.() -> Unit)? = null
) : GuiComponent<S>, ItemHolder {

    override var item: ItemStack? = item

    /**
     * Change the action of this button
     *
     * @param action the new click action.
     */
    public fun action(action: GuiView<S>.() -> Unit): Unit = run { this.action = action }

    public override fun apply(gui: GUI<S>, location: GuiLocation): Unit = gui.run {
        location.element(this@ComponentButton.item) {
            onClick { _, _ -> this@ComponentButton.action?.invoke(this) }
        }
    }
}
