package io.github.commandertvis.plugin

import io.github.commandertvis.plugin.gui.dsl.GUI
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Creates a [GUI] and applies an extension function.
 *
 * @param rows the quantity of rows of the GUI.
 * @param freezesPlayerInventory configures, should this GUI disable interaction with the player's inventory.
 * @param block the configuring function.
 * @return the new GUI archetype.
 */
public inline fun gui(rows: Number, freezesPlayerInventory: Boolean = true, block: GUI<Unit>.() -> Unit): GUI<Unit> {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    return GUI(rows.toByte(), freezesPlayerInventory, session = Unit).apply {
        block()
        initializeInventory()
    }
}

/**
 * Creates a [GUI] and applies an extension function.
 *
 * @param S the GUI's session type.
 * @param rows the quantity of rows of the GUI.
 * @param freezesPlayerInventory configures, should this GUI disable interaction with the player's inventory.
 * @param block the configuring function.
 * @return the new GUI archetype.
 */
public inline fun <reified S> sessionGui(
    rows: Number,
    freezesPlayerInventory: Boolean = true,
    defaultSession: S,
    block: GUI<S>.() -> Unit
): GUI<S> {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    return GUI(rows.toByte(), freezesPlayerInventory, session = defaultSession).apply {
        block()
        initializeInventory()
    }
}
