package io.github.commandertvis.plugin.gui.dsl

import io.github.commandertvis.plugin.gui.ItemHolder
import io.github.commandertvis.plugin.isPickupAction
import io.github.commandertvis.plugin.isPlaceAction
import org.bukkit.event.inventory.ClickType
import org.bukkit.event.inventory.InventoryAction
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryEvent
import org.bukkit.inventory.ItemStack

/**
 * Represents an element of [GUI] structure. Its facade is an item stack, and it handles inventory events, related to
 * the stack.
 *
 *  @param S the type of session of GUI that holds this element.
 */
@GuiDslMarker
public data class GuiElement<S> internal constructor(
    /**
     * The [ItemStack] that represents this element.
     */
    public override var item: ItemStack? = null,
    /**
     * The handler, fired when this [GuiElement] is loaded by the [GuiView].
     */
    public var onLoaded: (GuiView<S>.() -> Unit)? = null,
    /**
     * The handler, fired when the [GuiView], showing this [GuiElement], closes.
     */
    public var onClosing: (GuiView<S>.() -> Unit)? = null,

    @PublishedApi
    internal var handlersMap:
    Map<(event: InventoryEvent) -> Boolean, List<(event: InventoryEvent, guiView: GuiView<S>) -> Unit>> = hashMapOf()
) : ItemHolder {

    /**
     * Creates this element with `null` handlers and empty handlers map.
     *
     * @param item the [ItemStack] that represents this element.
     */
    public constructor(item: ItemStack? = null) : this(item, null)

    @PublishedApi
    internal fun handle(
        predicate: (event: InventoryEvent) -> Boolean,
        handler: (event: InventoryEvent, guiView: GuiView<S>) -> Unit
    ) {
        handlersMap = handlersMap.toMutableMap()
            .also { it[predicate] = it.getOrPut(predicate, ::mutableListOf).toMutableList().apply { add(handler) } }
    }

    /**
     * Adds a raw handler to any click on this element.
     */
    public inline fun onClickRaw(crossinline handler: (event: InventoryClickEvent, guiView: GuiView<S>) -> Unit): Unit =
        handle(
            { it is InventoryClickEvent },
            { event, guiView -> handler(event as InventoryClickEvent, guiView) }
        )

    /**
     * Changes the handler, fired when this [GuiElement] is loaded by the [GuiView].
     *
     * @param handler the new handler function.
     */
    public fun onLoaded(handler: GuiView<S>.() -> Unit): Unit = run { onLoaded = handler }

    /**
     * Changes the handler, fired when the [GuiView], showing this [GuiElement], closes.
     */
    public fun onClosing(handler: GuiView<S>.() -> Unit): Unit = run { onClosing = handler }

    /**
     * Adds a handler to any click on this element.
     *
     * @param handler the handling function.
     */
    public inline fun onClick(
        crossinline handler: GuiView<S>.(click: ClickType, action: InventoryAction) -> Unit
    ): Unit = onClickRaw { event, guiView -> guiView.handler(event.click, event.action) }

    /**
     * Adds a handler to left click on this element.
     *
     * @param handler the handling function.
     */
    public inline fun onLeftClick(crossinline handler: GuiView<S>.(action: InventoryAction) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.click == ClickType.LEFT)
                guiView.handler(event.action)
        }

    /**
     * Adds a handler to double-click on this element.
     *
     * @param handler the handling function.
     */
    public inline fun onDoubleClick(crossinline handler: GuiView<S>.(action: InventoryAction) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.click == ClickType.DOUBLE_CLICK)
                guiView.handler(event.action)
        }

    /**
     * Adds a handler to shift left click on this element.
     *
     * @param handler the handling function.
     */
    public inline fun onShiftLeftClick(crossinline handler: GuiView<S>.(action: InventoryAction) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.click == ClickType.SHIFT_LEFT)
                guiView.handler(event.action)
        }

    /**
     * Adds a handler to right click on this element.
     *
     * @param handler the handling function.
     */
    public inline fun onRightClick(crossinline handler: GuiView<S>.(action: InventoryAction) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.click == ClickType.RIGHT)
                guiView.handler(event.action)
        }

    /**
     * Adds a handler to shift right click on this element.
     *
     * @param handler the handling function.
     */
    public inline fun onShiftRightClick(crossinline handler: GuiView<S>.(action: InventoryAction) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.click == ClickType.SHIFT_RIGHT)
                guiView.handler(event.action)
        }

    /**
     * Adds a handler to middle click on this element.
     *
     * @param handler the handling function.
     */
    public inline fun onMiddleClick(crossinline handler: GuiView<S>.(action: InventoryAction) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.click == ClickType.MIDDLE)
                guiView.handler(event.action)
        }

    /**
     * Adds a handler to any click on this element that places an [ItemStack].
     *
     * @param handler the handling function.
     */
    public inline fun onPlaceItem(crossinline handler: GuiView<S>.(click: ClickType, item: ItemStack) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.action.isPlaceAction())
                guiView.handler(event.click, checkNotNull(event.cursor))
        }

    /**
     * Adds a handler to left click on this element that places an [ItemStack].
     *
     * @param handler the handling function.
     */
    public inline fun onPlaceItemLeftClick(crossinline handler: GuiView<S>.(item: ItemStack) -> Unit): Unit =
        onPlaceItem { click, item ->
            if (click == ClickType.LEFT)
                handler(item)
        }

    /**
     * Adds a handler to right click on this element that places an [ItemStack].
     *
     * @param handler the handling function.
     */
    public inline fun onPlaceItemRightClick(crossinline handler: GuiView<S>.(item: ItemStack) -> Unit): Unit =
        onPlaceItem { click, item ->
            if (click == ClickType.RIGHT)
                handler(item)
        }

    /**
     * Adds a handler to any click on this element that picks an [ItemStack] up.
     *
     * @param handler the handling function.
     */
    public inline fun onPickupItem(crossinline handler: GuiView<S>.(click: ClickType, item: ItemStack) -> Unit): Unit =
        onClickRaw { event, guiView ->
            if (event.action.isPickupAction())
                guiView.handler(event.click, checkNotNull(event.currentItem))
        }

    /**
     * Adds a handler to left click on this element that picks an [ItemStack] up.
     *
     * @param handler the handling function.
     */
    public inline fun onPickupItemLeftClick(crossinline handler: GuiView<S>.(item: ItemStack) -> Unit): Unit =
        onPickupItem { click, item ->
            if (click == ClickType.LEFT)
                handler(item)
        }

    /**
     * Adds a handler to right click on this element that picks an [ItemStack] up.
     *
     * @param handler the handling function.
     */
    public inline fun onPickupItemRightClick(crossinline handler: GuiView<S>.(item: ItemStack) -> Unit): Unit =
        onPickupItem { click, item ->
            if (click == ClickType.RIGHT)
                handler(item)
        }

    /**
     * Returns an exact copy of this [GuiElement].
     *
     * @return the exact copy of this [GuiElement].
     */
    public fun copy(): GuiElement<S> = copy(
        item = item?.clone(),

        handlersMap = hashMapOf
        <(event: InventoryEvent) -> Boolean, MutableList<(event: InventoryEvent, guiView: GuiView<S>) -> Unit>>().also {
            handlersMap.forEach { (k, v) -> it[k] = v.mapTo(mutableListOf(), { it }) }
        }
    )
}
