package io.github.commandertvis.plugin.gui

import io.github.commandertvis.plugin.gui.dsl.GUI
import io.github.commandertvis.plugin.gui.dsl.GuiView
import org.bukkit.entity.Player

/**
 * Returns the opened by this player GUI view if there is.
 */
public inline val Player.openedGuiView: GuiView<*>?
    get() = guiViews.find { it.playerID == uniqueId }

/**
 * Shows a [GUI] to a [Player].
 *
 * @param S the type of session that belongs to the GUI.
 * @return the [GuiView], created after showing.
 */
public fun <S> Player.showGui(gui: GUI<S>): GuiView<S> {
    closeGuis()
    return GuiView(gui.copy(), uniqueId)
}

/**
 * Closes a GUI of [Player] if it is equals to the given one.
 *
 * @param gui the GUI to close.
 */
public fun Player.closeGui(gui: GUI<*>): Unit =
    run { guiViews.find { it._gui == gui && it.playerID == uniqueId }?.close() }

/**
 * Closes opened GUI of certain [Player].
 */
public fun Player.closeGuis(): Unit = run { openedGuiView?.close() }
