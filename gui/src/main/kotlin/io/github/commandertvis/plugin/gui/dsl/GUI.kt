package io.github.commandertvis.plugin.gui.dsl

import io.github.commandertvis.plugin.createInventory
import io.github.commandertvis.plugin.gui.GuiLocation
import io.github.commandertvis.plugin.gui.dsl.components.ComponentButton
import io.github.commandertvis.plugin.gui.dsl.components.ComponentLinearAnimatedIcon
import io.github.commandertvis.plugin.gui.dsl.components.GuiComponent
import org.bukkit.Material
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Represents a GUI, a structure that can be turned into [Inventory] that can be shown to a player.
 *
 * @param S the type of session, held by this [GUI].
 * @throws IllegalArgumentException when either quantity of [rows] is less than 1 or more than 6.
 */
@GuiDslMarker
public data class GUI<S> @PublishedApi internal constructor(
    /**
     * The quantity of rows that the inventory of this GUI should have. It mustn't be less than 1 or more than 6.
     */
    public val rows: Byte,
    /**
     * Should this [GUI] disable interaction with a player's inventory.
     */
    public val freezesPlayerInventory: Boolean = true,

    @PublishedApi
    internal var elements: Map<GuiLocation, GuiElement<S>> = emptyMap(),

    /**
     * The title [String] that the [Inventory] of this [GUI] should have.
     */
    public var title: String? = null,

    /**
     * Session properties, separate for each [GUI] instance.
     */
    public var session: S,

    /**
     * The handler, fired when this [GUI] is loaded by the [GuiView].
     */
    public var onLoaded: (GuiView<S>.() -> Unit)? = null,

    /**
     * The handler, fired when the [GUI], showing this [GuiElement], closes.
     */
    public var onClosing: (GuiView<S>.() -> Unit)? = null
) {

    /**
     * The state is this [GUI] currently shown to a player.
     */
    public var isShown: Boolean = false
        internal set

    /**
     * The [Inventory] that represents this [GUI].
     */
    public lateinit var inventory: Inventory
        private set

    init {
        require(rows in 1..6) { "GUI must have from 1 to 6 rows" }
    }

    @PublishedApi
    internal fun initializeInventory() {
        val size = rows * 9
        val title1 = title
        val inventory = if (title1 == null) createInventory(size) else createInventory(size, title1)
        this.inventory = inventory
        updateInventory()
    }

    @PublishedApi
    internal fun updateInventory(): Unit = elements.forEach { (k, v) -> inventory.setItem(k.toChestSlot(), v.item) }

    /**
     * Changes the handler, fired when this [GUI] is loaded by the [GuiView].
     *
     * @param handler the new handler function.
     */
    public fun onLoaded(handler: GuiView<S>.() -> Unit): Unit = run { onLoaded = handler }

    /**
     * Changes the handler, fired when the [GUI], showing this [GuiElement], closes.
     */
    public fun onClosing(handler: GuiView<S>.() -> Unit): Unit = run { onClosing = handler }


    /**
     * Applies a [GuiComponent] to this [GUI] in needed [GuiLocation].
     *
     * @param guiComponent the component to apply.
     */
    public infix fun GuiLocation.component(guiComponent: GuiComponent<S>): Unit = guiComponent.apply(this@GUI, this)

    /**
     * Creates or replaces the [GuiElement] at the [GuiLocation].
     *
     * @param item the [ItemStack] that represents the element.
     */
    public infix fun GuiLocation.element(item: ItemStack): Unit = element(item, null)

    /**
     * Creates or replaces with empty one the [GuiElement] at the [GuiLocation] and applies an extension function.
     *
     * @param block the extension function to apply.
     */
    public infix fun GuiLocation.element(block: GuiElement<S>.() -> Unit): Unit = element(null, block)

    /**
     * Creates or replaces the [GuiElement] at the [GuiLocation] and applies an extension function.
     *
     * @param item the [ItemStack] that represents the element.
     * @param block the extension function to apply.
     */
    public fun GuiLocation.element(item: ItemStack?, block: (GuiElement<S>.() -> Unit)?) {
        elements = elements.toMutableMap().also {
            val e = GuiElement<S>(item)
            block?.let { it1 -> e.it1() }
            it[this] = e
        }
    }

    /**
     * Creates or updates the [GuiElement] at the [GuiLocation] and applies an extension function.
     *
     * @param defaultItem the [ItemStack] that will be set for a new element if this location contains no element.
     * @param action the extension function to apply.
     */
    public inline fun GuiLocation.update(defaultItem: ItemStack?, action: GuiElement<S>.() -> Unit) {
        contract { callsInPlace(action, InvocationKind.AT_MOST_ONCE) }
        elements = elements.toMutableMap().also { it.getOrPut(this) { GuiElement(defaultItem) }.action() }
    }

    /**
     * Updates the [GuiElement] at the [GuiLocation] if it exists by applying an extension function.
     *
     * @param action the extension function to apply.
     */
    public inline infix fun GuiLocation.update(action: GuiElement<S>.() -> Unit) {
        contract { callsInPlace(action, InvocationKind.AT_MOST_ONCE) }
        elements[this]?.action()
    }

    /**
     * Creates or replaces the elements of the row of this [Number].
     *
     * @param item the [ItemStack] that represents the element.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public infix fun Number.row(item: ItemStack): Unit = row(item, null)

    /**
     * Creates or replaces the elements with empty ones of the row of this [Number] and applies an extension function
     * to each.
     *
     * @param block the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public infix fun Number.row(block: GuiElement<S>.() -> Unit): Unit =
        row(null, block)

    /**
     * Creates or replaces the elements of the row of this [Number] and applies an extension function to each.
     *
     * @receiver the row coordinate.
     * @param item the [ItemStack] that represents the element.
     * @param block the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public fun Number.row(item: ItemStack?, block: (GuiElement<S>.() -> Unit)?): Unit =
        forEachRow { it.element(item, block) }

    /**
     * Creates or updates the elements of the row of this [Number] and applies an extension function to each.
     *
     * @receiver the row coordinate.
     * @param defaultItem the [ItemStack] that will be set for a new element if this location contains no element.
     * @param action the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public inline fun Number.updateRow(defaultItem: ItemStack?, action: GuiElement<S>.() -> Unit): Unit =
        forEachRow { it.update(defaultItem, action) }

    /**
     * Updates the elements of the row of this [Number] if it exists by applying an extension function to each.
     *
     * @receiver the row coordinate.
     * @param action the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public inline infix fun Number.updateRow(action: GuiElement<S>.() -> Unit): Unit = forEachRow { it.update(action) }

    /**
     * Creates or replaces the elements of the column of this [Number].
     *
     * @param item the [ItemStack] that represents the element.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public infix fun Number.column(item: ItemStack): Unit = column(item, null)

    /**
     * Creates or replaces the elements with empty ones of the column of this [Number] and applies an extension function
     * to each.
     *
     * @param block the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public infix fun Number.column(block: GuiElement<S>.() -> Unit): Unit = column(null, block)

    /**
     * Creates or replaces the elements of the column of this [Number] and applies an extension function to each.
     *
     * @receiver the column coordinate.
     * @param item the [ItemStack] that represents the element.
     * @param block the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public fun Number.column(item: ItemStack?, block: (GuiElement<S>.() -> Unit)?): Unit =
        forEachColumn { it.element(item, block) }

    /**
     * Creates or updates the elements of the column of this [Number] and applies an extension function to each.
     *
     * @receiver the column coordinate.
     * @param defaultItem the [ItemStack] that will be set for a new element if this location contains no element.
     * @param action the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public inline fun Number.updateColumn(defaultItem: ItemStack?, action: GuiElement<S>.() -> Unit): Unit =
        forEachColumn { it.update(defaultItem, action) }

    /**
     * Updates the elements of the column of this [Number] if it exists by applying an extension function to each.
     *
     * @receiver the column coordinate.
     * @param action the extension function to apply.
     * @throws IllegalArgumentException when the needed row doesn't exist in this [GUI].
     */
    public inline infix fun Number.updateColumn(action: GuiElement<S>.() -> Unit): Unit =
        forEachColumn { it.update(action) }

    /**
     * Creates or replaces the elements of the rectangle, built by two points, and applies an extension function to
     * each.
     *
     * @param a the location of first point.
     * @param b the location of second point.
     * @param item the [ItemStack] that represents the element.
     * @param block the extension function to apply.
     */
    public fun rectangle(
        a: GuiLocation,
        b: GuiLocation,
        item: ItemStack? = null,
        block: (GuiElement<S>.() -> Unit)? = null
    ): Unit =
        forEachRectangle(a, b) { it.element(item, block) }

    /**
     * Creates or updates the elements of the rectangle, built by two points, and applies an extension function to
     * each.
     *
     * @param a the location of first point.
     * @param b the location of second point.
     * @param defaultItem the [ItemStack] that will be set for a new element if this location contains no element.
     * @param action the extension function to apply.
     */
    public inline fun updateRectangle(
        a: GuiLocation,
        b: GuiLocation,
        defaultItem: ItemStack? = null,
        action: GuiElement<S>.() -> Unit
    ): Unit = forEachRectangle(a, b) { it.update(defaultItem, action) }

    /**
     * Updates the elements of the rectangle, built by two points, by applying an extension function to each.
     *
     * @param a the location of first point.
     * @param b the location of second point.
     * @param action the extension function to apply.
     */
    public inline fun updateRectangle(a: GuiLocation, b: GuiLocation, action: GuiElement<S>.() -> Unit): Unit =
        forEachRectangle(a, b) { it.update(action) }

    /**
     * Removes an element from this [GuiLocation] if there is.
     */
    public fun GuiLocation.delete(): Unit = run { elements = elements.toMutableMap().also { it.remove(this) } }

    /**
     * Removes all the elements of the row of this [Number].
     *
     * @receiver the row coordinate.
     */
    public fun Number.deleteRow(): Unit = forEachRow { it.delete() }

    /**
     * Removes all the elements of the column of this [Number].
     */
    public fun Number.deleteColumn(): Unit = forEachColumn { it.delete() }

    /**
     * Removes all the elements of the rectangle, built by two points.
     *
     * @param a the first point's [GuiLocation].
     * @param b the second point's location.
     */
    public fun deleteRectangle(a: GuiLocation, b: GuiLocation): Unit = forEachRectangle(a, b) { it.delete() }

    /**
     * Moves an element if it exists from this [GuiLocation] to another one.
     *
     * @param location the location to move to.
     */
    public fun GuiLocation.moveTo(location: GuiLocation) {
        if (this in elements)
            elements = elements.toMutableMap().also { it[location] = checkNotNull(it[this]) }
    }

    /**
     * Creates a [GuiLocation] in this [GUI] where the first number is for x-coordinate and second number is for
     * y-coordinate.
     *
     * @receiver the x-coordinate.
     * @param y the y-coordinate.
     * @return the new [GuiLocation].
     */
    public infix fun Number.on(y: Number): GuiLocation = GuiLocation(this@GUI, toByte(), y.toByte())

    /**
     * Iterates through all the locations of the row of this [Number].
     *
     * @receiver the row coordinate.
     * @param action the action to apply to each location.
     */
    public inline fun Number.forEachRow(action: (GuiLocation) -> Unit): Unit = repeat(9) { action(this on it) }

    /**
     * Iterates through all the locations of the column of this [Number].
     *
     * @receiver the column coordinate.
     * @param action the action to apply to each location.
     */
    public inline fun Number.forEachColumn(action: (GuiLocation) -> Unit): Unit =
        repeat(rows.toInt()) { action(it on this) }

    /**
     * Iterates through all the locations of the rectangle, built by two points.
     *
     * @param a the first point's [GuiLocation].
     * @param b the second point's location.
     * @param action the action to apply to each location.
     */
    public inline fun forEachRectangle(a: GuiLocation, b: GuiLocation, action: (GuiLocation) -> Unit): Unit =
        (a.x..b.x).forEach { x -> (a.y..b.y).forEach { y -> action(x on y) } }

    /**
     * Creates a button at certain location and applies a configuring function to it.
     *
     * @param item [ItemStack] that represent the button.
     * @param block the function to configure it.
     */
    public inline fun GuiLocation.button(item: ItemStack, block: ComponentButton<S>.() -> Unit): Unit =
        component(ComponentButton<S>(item).apply(block))

    /**
     * Creates an animated icon at certain location and applies a configuring function to it.
     *
     * @param intervalMillis the interval between updating the icon item.
     * @param items the items to roll during animation.
     * @param block the function to configure the icon.
     */
    public fun GuiLocation.linearAnimatedIcon(
        intervalMillis: Long,
        vararg items: ItemStack?,
        block: (ComponentLinearAnimatedIcon<S>.() -> Unit)? = null
    ): Unit = if (block == null)
        component(ComponentLinearAnimatedIcon(intervalMillis, items.asList()))
    else
        component(ComponentLinearAnimatedIcon<S>(intervalMillis, items.asList()).apply(block))

    /**
     * Creates an animated icon at certain location and applies a configuring function to it.
     *
     * @param intervalMillis the interval between updating the icon item.
     * @param materials the item materials to roll during animation.
     * @param block the function to configure the icon.
     */
    public fun GuiLocation.linearAnimatedIcon(
        intervalMillis: Long,
        vararg materials: Material,
        block: (ComponentLinearAnimatedIcon<S>.() -> Unit)? = null
    ): Unit =
        linearAnimatedIcon(intervalMillis, *materials.map { ItemStack(it) }.toTypedArray()) { block?.let { it() } }

    /**
     * Returns an exact copy of this [GUI].
     *
     * @return the exact copy of this [GUI].
     */
    public fun copy(): GUI<S> = copy(
        elements = hashMapOf<GuiLocation, GuiElement<S>>().also { elements.forEach { (k, v) -> it[k] = v.copy() } },
        session = session
    ).also { newGui ->
        val size = rows * 9
        val title1 = title

        newGui.inventory = (if (title1 == null) createInventory(size) else
            createInventory(size, title1)).also { newInventory ->
            inventory.forEachIndexed { index, stack -> stack?.let { newInventory.setItem(index, stack.clone()) } }
        }
    }

    /**
     * Retrieves a [GuiLocation] instance by the raw slot of an item in the chest [Inventory].
     *
     * @param slot the number of chest slot.
     * @return the new location.
     */
    public fun chestSlot(slot: Number): GuiLocation = GuiLocation(this, slot.toByte())

    public companion object {
        private const val serialVersionUID = 5427935450580790094L
    }
}
