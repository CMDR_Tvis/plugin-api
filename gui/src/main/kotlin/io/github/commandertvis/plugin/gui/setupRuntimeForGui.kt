package io.github.commandertvis.plugin.gui

import io.github.commandertvis.plugin.gui.dsl.GuiView
import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.handleCancellable
import io.github.commandertvis.plugin.isFilledUp
import org.bukkit.event.inventory.*
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.Plugin

private inline val InventoryEvent.guiView: GuiView<*>?
    get() = guiViews.find { it.playerID == view.player.uniqueId }

internal lateinit var runtimePlugin: Plugin

/**
 * Sets up necessary event handlers for GUI module's valid work. It must be used only in PluginApi runtime
 * implementation!
 */
@Suppress("UNCHECKED_CAST")
public fun Plugin.setupRuntimeForGui() {
    runtimePlugin = this

    handleCancellable(ignoreCancelled = true, action = fun InventoryClickEvent.() {
        val guiView = guiView ?: return

        if (rawSlot != view.convertSlot(rawSlot)) {
            if (!checkNotNull(guiView._gui).freezesPlayerInventory
                && click != ClickType.SHIFT_LEFT
                && click != ClickType.SHIFT_RIGHT
            ) {
                if (click == ClickType.DOUBLE_CLICK) {
                    val playerInventory = view.bottomInventory

                    playerInventory.getItem(slot)?.let { clicked ->
                        playerInventory.forEachIndexed { index, i ->
                            if (!clicked.isFilledUp() && index != slot && i?.isSimilar(clicked) == true) {
                                val diff = i.amount + clicked.amount

                                if (diff > clicked.maxStackSize) {
                                    val transaction = clicked.maxStackSize - diff
                                    i.amount -= transaction
                                    clicked.amount += transaction
                                } else {
                                    playerInventory.setItem(index, null)
                                    clicked.amount += i.amount
                                }
                            }
                        }
                    }
                }

                return
            }

            isCancelled = true
            return
        }

        isCancelled = true

        guiView._gui?.elements?.forEach { (location, element) ->
            if (location.toChestSlot() == rawSlot)
                element.handlersMap.forEach handlers@{ (predicate, handlers) ->
                    if (predicate(this))
                        (handlers as List<(event: InventoryEvent, GuiView<*>) -> Unit>).forEach { handler ->
                            handler(this, guiView)
                            return@handlers
                        }
                }
        }
    })

    handle<InventoryCloseEvent> { (guiView ?: return@handle).close() }
    handle<PlayerQuitEvent> { player.closeGuis() }

    handleCancellable(ignoreCancelled = true, action = fun InventoryDragEvent.() {
        val guiView = guiView ?: return
        if (rawSlots.none { it == view.convertSlot(it) } && !checkNotNull(guiView._gui).freezesPlayerInventory) return
        isCancelled = true
    })
}
