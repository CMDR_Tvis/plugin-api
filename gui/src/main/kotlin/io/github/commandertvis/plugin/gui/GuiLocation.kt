package io.github.commandertvis.plugin.gui

import io.github.commandertvis.plugin.gui.dsl.GUI
import java.io.Serializable
import kotlin.math.floor

/**
 * Represents a location of a slot in [GUI].
 */
public data class GuiLocation internal constructor(
    /**
     * The x-coordinate of this location.
     */
    public val x: Byte,

    /**
     * The y-coordinate of this location.
     */
    public val y: Byte
) : Serializable {

    internal constructor(gui: GUI<*>, x: Byte, y: Byte) : this(x, y) {
        require(gui.rows - 1 >= x) { "gui is too small for x=$x" }
    }

    internal constructor(gui: GUI<*>, chestSlot: Byte) : this(
        gui,
        floor(chestSlot.toFloat() / 9).toInt().toByte(),
        (chestSlot % 9).toByte()
    )

    init {
        require(8 >= y) { "gui is too small for y=$y" }
    }

    internal fun toChestSlot(): Int = x * 9 + y

    public companion object {
        private const val serialVersionUID = 6216898842485487799L
    }
}
