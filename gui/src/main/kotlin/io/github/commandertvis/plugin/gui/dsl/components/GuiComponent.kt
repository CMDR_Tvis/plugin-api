package io.github.commandertvis.plugin.gui.dsl.components

import io.github.commandertvis.plugin.gui.GuiLocation
import io.github.commandertvis.plugin.gui.dsl.GUI

/**
 * Represent a GUI component, a GUI element structure that is pre-configured before adding to GUI.
 *
 * @param S the type of session of GUIs, dedicated to this component.
 */
public interface GuiComponent<S> {
    /**
     * Attached the element to certain GUI in certain location.
     *
     * @param gui the GUI to build element for.
     * @param location the location where set the element up.
     */
    public fun apply(gui: GUI<S>, location: GuiLocation)
}
