package io.github.commandertvis.plugin.gui.dsl

import io.github.commandertvis.plugin.findPlayer
import io.github.commandertvis.plugin.gui.guiViews
import org.bukkit.entity.Player
import org.bukkit.inventory.InventoryView
import java.util.*

/**
 * Represent a GUI view: state when GUI object is displayed to player. It is constructed when the GUI is shown and
 * destructed when the GUI is closed.
 *
 * @param S the type of session, held by this [GuiView].
 */
@GuiDslMarker
public data class GuiView<S> internal constructor(
    @PublishedApi internal var _gui: GUI<S>?,
    @PublishedApi internal val playerID: UUID
) : AutoCloseable {
    /**
     * The [Player], working with this [GuiView]. Can be null.
     */
    public inline val playerUnsafe: Player?
        get() = findPlayer(playerID)

    /**
     * The [Player], working with this [GuiView]. Throws an exception if the player is not present.
     */
    public inline val player: Player
        get() = playerUnsafe ?: run {
            close()
            error("cannot find GuiView player (id $playerID), but GuiView wasn't closed")
        }

    /**
     * The [GUI] object, shown in this [GuiView].
     */
    public inline val gui: GUI<S>
        get() = _gui!!

    /**
     * State, is this GUI shown to a player.
     */
    public var isShown: Boolean = true
        private set

    /**
     * The [InventoryView], used to display the GUI.
     */
    public var inventoryView: InventoryView? = null

    init {
        guiViews += this

        run {
            (_gui ?: let {
                it.close()
                return@run
            }).let { gui ->
                inventoryView = player.openInventory(gui.inventory)
                gui.initializeInventory()
                gui.isShown = true
                gui.elements.forEach { (_, v) -> v.onLoaded?.invoke(this) }
                gui.onLoaded?.let { handler -> handler() }
            }
        }
    }

    /**
     * Applies an action to the [GUI] of this [GuiView]. It also calls [updateContents] after executing [action].
     *
     * @param action the action to apply.
     */
    public fun gui(action: GUI<S>.() -> Unit) {
        gui.action()
        updateContents()
    }

    /**
     * Closes this view or handles this GUI inventory's closing. [GuiView] mustn't be used or accessed after using this
     * function.
     */
    public override fun close() {
        playerUnsafe?.updateInventory()

        _gui?.let { gui ->
            gui.elements.forEach { (_, v) -> v.onClosing?.let { it() } }
            gui.onClosing?.let { it() }
        }

        guiViews -= this
        inventoryView?.close()
        inventoryView = null
        _gui = null
    }

    /**
     * Calls [close] function.
     */
    protected fun finalize(): Unit = close()

    /**
     * Updates the representation of elements of this GUI.
     */
    public fun updateContents() {
        gui.updateInventory()
        inventoryView?.topInventory?.contents = gui.inventory.contents
    }

    /**
     * Returns an exact copy of this [GuiView].
     *
     * @return the exact copy of this [GuiView].
     */
    public fun copy(): GuiView<S> = copy(_gui = _gui?.copy())
}
