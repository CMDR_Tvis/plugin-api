package io.github.commandertvis.plugin.gui

import io.github.commandertvis.plugin.getOrCreateLore
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemFlag
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta

/**
 * Represent a structure that contains an [ItemStack], it provides some utilities for configuring and interaction with
 * this item.
 */
public interface ItemHolder {
    /**
     * The [ItemStack], held by this [ItemHolder].
     */
    public var item: ItemStack?

    /**
     * The [ItemMeta] of [item].
     */
    public var meta: ItemMeta?
        get() = item?.itemMeta
        set(value) = run { item?.itemMeta = value }

    /**
     * The map of enchantments of [item].
     */
    public var enchantment: MutableMap<Enchantment, Int>?
        get() = meta?.enchants
        set(value) {
            val meta = meta ?: return
            meta.enchants.clear()
            value?.forEach { (k, v) -> meta.addEnchant(k, v, true) }
            this.meta = meta
        }

    /**
     * The display name of [item].
     */
    public var name: String?
        get() = meta?.displayName
        set(value) = run {
            val meta = meta ?: return
            meta.setDisplayName(value)
            this.meta = meta
        }

    /**
     * The lore of [item].
     */
    public var lore: List<String>?
        get() = meta?.getOrCreateLore()
        set(value) = run {
            val meta = meta ?: return
            meta.lore = value
            this.meta = meta
        }

    /**
     * The flags of [item].
     */
    public var flags: Set<ItemFlag>?
        get() = meta?.itemFlags
        set(value) {
            val meta = meta ?: return
            value ?: return
            meta.removeItemFlags(*flags?.toTypedArray()!!)
            meta.addItemFlags(*value.toTypedArray())
            this.meta = meta
        }

    /**
     * The flag, is the [item] unbreakable. Setting this property `null` does nothing.
     */
    public var isUnbreakable: Boolean?
        get() = meta?.isUnbreakable
        set(value) {
            val meta = meta ?: return
            value ?: return
            meta.isUnbreakable = value

        }
}
