package io.github.commandertvis.plugin.gui.dsl

@DslMarker
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS)
internal annotation class GuiDslMarker
